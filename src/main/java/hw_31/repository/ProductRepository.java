package hw_31.repository;

import hw_31.entity.Product;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

import static java.util.Objects.isNull;

@Component
public class ProductRepository {
    private final Map<UUID, Product> storage;

    public boolean save(Product product) {

        if (isNull(product)) {
            throw new IllegalArgumentException("Product can't be null!");
        }

        if (isNull(product.getId())) {
            throw new IllegalArgumentException("UUID of Product can't be null!");
        }

        if (storage.containsKey(product.getId())) {
            throw new IllegalArgumentException("Product with id "+product.getId()+" is already in storage!");
        }

        putInternal(product);

        return true;
    }

    private void putInternal(Product product) {
        storage.put(product.getId(), product);
    }

    public Optional<Product> findById(UUID uuid) {
        return Optional.ofNullable(storage.get(uuid));
    }

    public List<Product> findAll() {

        return new ArrayList<>(storage.values());
    }

    public ProductRepository() {
        this.storage = new HashMap<>();
    }

    @Override
    public String toString() {
        return "ProductRepository{" +
                "storage=" + storage +
                '}';
    }

    @PostConstruct
    public void init() {
        Product product1 = new Product(UUID.randomUUID(), "М'яч футбольний", new BigDecimal("100.34"));
        Product product2 = new Product(UUID.randomUUID(), "М'яч волейбольний", new BigDecimal("75"));
        Product product3 = new Product(UUID.randomUUID(), "М'яч тенісний великий", new BigDecimal("50.5"));
        Product product4 = new Product(UUID.randomUUID(), "М'яч тенісний малий", new BigDecimal("10.12"));
        Product product5 = new Product(UUID.randomUUID(), "М'яч баскетбольний", new BigDecimal("115.75"));

        putInternal(product1);
        putInternal(product2);
        putInternal(product3);
        putInternal(product4);
        putInternal(product5);
    }
}
