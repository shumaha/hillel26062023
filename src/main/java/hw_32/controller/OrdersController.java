package hw_32.controller;

import hw_32.dto.OrderCreateDto;
import hw_32.entity.Order;
import hw_32.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrdersController {

    @Autowired
    OrderService orderService;
    Logger logger = LoggerFactory.getLogger(OrdersController.class);

    @GetMapping("/show")
    public List<Order> showOrders() {
        logger.info("Request /orders/show");

        return orderService.showAll();
    }

    @GetMapping("/show/{id}")
    public Order showOrder(@PathVariable("id") int idProduct) {
        logger.info("Request /orders/show/"+idProduct);

        return orderService.findOrderById(idProduct);
    }

    @PostMapping("/create")
    public Order createOrder(@RequestBody OrderCreateDto orderCreateDto) {
        logger.info("Request /orders/create. Input data: " + orderCreateDto);

        return orderService.addOrder(orderCreateDto.getDate(), orderCreateDto.getProducts());
    }
}
