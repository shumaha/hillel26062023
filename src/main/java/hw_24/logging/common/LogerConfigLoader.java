package hw_24.logging.common;

import java.io.IOException;

public abstract class LogerConfigLoader<T extends LoggerConfiguration> {

    public abstract T load(String resource) throws IOException;
}
