package hw_19.abstractFactory.factories;



public abstract class Music {
    private final String type;
    protected Music(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Music{" +
                "type='" + type + '\'' +
                '}';
    }
}
