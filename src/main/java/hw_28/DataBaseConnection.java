package hw_28;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public final class DataBaseConnection {

    private static Connection connection;
    private static final Properties PROPERTIES = new Properties();

    private static String ip;
    private static int port;
    private static String username;
    private static String password;

    static {
        try {
            PROPERTIES.load(new FileInputStream("src/main/resources/application.properties"));
            ip = PROPERTIES.getProperty("ip");
            port = Integer.parseInt(PROPERTIES.getProperty("port"));
            username = PROPERTIES.getProperty("username");
            password = PROPERTIES.getProperty("password");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private DataBaseConnection() {}

    public static synchronized Connection getConnection() throws SQLException {

        if (connection == null || connection.isClosed()) {
            connection = DriverManager.getConnection("jdbc:mysql://"+ip+":"+port+"/hw_28", username, password);
        }

        return connection;
    }

    public static synchronized void closeConnection(Connection conn) throws SQLException {
        conn.close();
    }
}
