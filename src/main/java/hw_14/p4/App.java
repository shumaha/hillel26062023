package hw_14.p4;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class App {
    public static void main(String[] args) {
        List<Product> productList = generateProductList();

        System.out.println("Original list: " + productList);
        System.out.println("Filtered list:  " + getLastCreated(productList));
    }

    private static List<Product> generateProductList() {
        List<Product> generatedList = new ArrayList<>();

        generatedList.add(new Product(Product.Type.Book, 256, LocalDate.parse("2023-08-26")));
        generatedList.add(new Product(Product.Type.Toy, 300, LocalDate.parse("2023-06-03")));
        generatedList.add(new Product(Product.Type.Toy, 78, LocalDate.parse("2023-04-24")));
        generatedList.add(new Product(Product.Type.Book, 100, LocalDate.parse("2023-07-17")));

        Product discountedProduct1 = new Product(Product.Type.Toy, 150); discountedProduct1.enableDiscount(10);
        generatedList.add(discountedProduct1);

        Product discountedProduct2 = new Product(Product.Type.Toy, 700, LocalDate.parse("2023-01-15")); discountedProduct2.enableDiscount(40);
        generatedList.add(discountedProduct2);

        Product discountedProduct3 = new Product(Product.Type.Book, 356, LocalDate.parse("2023-02-13")); discountedProduct3.enableDiscount(10);
        generatedList.add(discountedProduct3);


        return generatedList;
    }

    private static List<Product> getLastCreated(List<Product> inList) {

        return inList.stream()
                .sorted(Comparator.comparing((Product::getCreateDate)).reversed())
                .limit(3)
                .collect(Collectors.toList());
    }


}
