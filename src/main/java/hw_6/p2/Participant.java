package hw_6.p2;

/**
 * Учасник змагань
 */
public abstract class Participant {
    private final String name;
    private final int MAX_RUN;
    private final int MAX_JUMP;
    protected int maxJumpCount;

    public Participant(String name, int maxRun, int maxJump, int maxJumpCount) {
        this.name = name;
        this.MAX_RUN = maxRun;
        this.MAX_JUMP = maxJump;
        this.maxJumpCount = maxJumpCount;
    }

    public int getMaxRun() {
        return MAX_RUN;
    }

    public int getMaxJump() {
        return MAX_JUMP;
    }

    public abstract String getLabel();

    public int getMaxJumpCount() {
        return maxJumpCount;
    }

    public abstract void run();

    public void jump() {
        maxJumpCount--;
    }

    public String toString() {
        return name + "("+this.getClass().getSimpleName()+")";
    }
}
