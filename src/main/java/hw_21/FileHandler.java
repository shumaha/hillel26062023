package hw_21;

import java.io.*;

public abstract class FileHandler {

    private final String fileContent;
    private final String pathFileToWrite;

    public FileHandler(String pathFileToRead, String pathFileToWrite) {


        try (BufferedReader br = new BufferedReader(new FileReader(pathFileToRead))) {
            String line;
            StringBuilder sb = new StringBuilder();

            while ( (line = br.readLine()) != null) {
                sb.append(line).append(System.lineSeparator());
            }

            fileContent = sb.toString();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }


        this.pathFileToWrite = pathFileToWrite;
    }

    public String getFileContent() {
        return fileContent;
    }

    protected void writeResult(String result) {

        try (FileWriter fileWriter = new FileWriter(pathFileToWrite, false)) {
            fileWriter.append(result);
            fileWriter.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public abstract String handleFileContent();
}
