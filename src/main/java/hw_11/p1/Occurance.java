package hw_11.p1;

public record Occurance(String name,int occurrence) {

    @Override
    public String toString() {
        return "{" +
                "name: \"" + name + '\"' +
                ", occurrence: " + occurrence +
                '}';
    }
}
