package hw_14.p5;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Product {

    final public int id;
    final private Type type;
    private double price;

    private boolean discountEnable = false;

    private int discountValue = 0;

    private LocalDate createDate;

    public Product(Type type, int price) {
        this.id = ProductIdGenerator.getNextId();
        this.type = type;
        this.price = price;
        createDate = LocalDate.now();
    }

    public Product(Type type, int price, LocalDate createDate) {
        this(type, price);
        this.createDate = createDate;
    }


    public int getId() {
        return id;
    }

    public Type getType() {
        return type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public double getFinalPrice() {

        return discountEnable ? price - (price * discountValue/100) : price;
    }

    public int getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(int discountValue) {
        if (!discountEnable) {
            throw new IllegalArgumentException("Discount is not enabled!");
        }

        if (1 > discountValue || discountValue > 99) {
            throw new IllegalArgumentException("Discount value can be from 1 to 99%!");
        }

        this.discountValue = discountValue;
    }

    public void enableDiscount(int discountValue) {
        if (1 > discountValue || discountValue > 99) {
            throw new IllegalArgumentException("Discount value can be from 1 to 99%!");
        }

        this.discountEnable = true;
        this.discountValue = discountValue;
    }

    public void disableDiscount() {
        this.discountEnable = false;
        this.discountValue = 0;
    }

    public boolean isDiscountEnable() {
        return discountEnable;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public enum Type {
        Book,
        Toy
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", type=" + type +
                ", price=" + getFinalPrice() +
                (discountEnable ? ", discount: "+discountEnable+" ("+discountValue +  "% off from "+price+")" : "") +
                ", createDate=" + createDate.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")) +
                '}';
    }
}