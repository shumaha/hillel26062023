package hw_18.observer;

import hw_18.observer.interfaces.Observable;

import java.util.ArrayList;
import java.util.List;

public class AirAlertMessengerBot implements Observable<MessengerAccount, AlarmEvent> {

    private final List<MessengerAccount> accounts;

    public AirAlertMessengerBot() {
        this.accounts = new ArrayList<>();
    }

    @Override
    public void addObserver(MessengerAccount account) {
        accounts.add(account);
    }

    @Override
    public void deleteObserver(MessengerAccount account) {
        accounts.remove(account);
    }

    @Override
    public void notifyObservers(AlarmEvent alarmEvent) {
        accounts.forEach(a -> a.handleEvent(alarmEvent));
    }
}
