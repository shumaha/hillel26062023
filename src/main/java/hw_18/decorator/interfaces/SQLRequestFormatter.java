package hw_18.decorator.interfaces;

public interface SQLRequestFormatter {

    String format(String sqlRequest);
}
