package hw_18.observer.interfaces;

public interface Observable<T extends Observer<E>, E extends Event> {

    void addObserver(T t);
    void deleteObserver(T t);

    void notifyObservers(E e);
}
