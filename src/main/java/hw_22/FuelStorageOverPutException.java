package hw_22;

public class FuelStorageOverPutException extends Exception {
    public FuelStorageOverPutException(String message) {
        super(message);
    }
}
