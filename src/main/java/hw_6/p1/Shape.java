package hw_6.p1;

public interface Shape {

    double getArea();
}
