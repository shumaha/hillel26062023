package hw_19.abstractFactory.factories;

public class LocalVideo extends Video {

    protected LocalVideo() {
        super("Local");
    }
}
