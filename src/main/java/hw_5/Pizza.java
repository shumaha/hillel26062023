package hw_5;

import java.util.Objects;

public class Pizza {

    private final boolean dough; // тісто
    private final MeatType meatType;
    private final int meatCnt;
    private final CheeseType cheeseType;
    private final int cheeseCnt;
    private final MashroomsType mashroomsType;
    private final boolean tomato;
    private final boolean sauce;


    private Pizza(int meatCnt, MeatType meatType, int cheeseCnt, CheeseType cheeseType, MashroomsType mashroomsType, boolean tomato, boolean sauce) {
        this.dough = true;
        this.meatCnt = meatCnt;
        this.meatType = meatType;
        this.cheeseCnt = cheeseCnt;
        this.cheeseType = cheeseType;
        this.mashroomsType = mashroomsType;
        this.tomato = tomato;
        this.sauce = sauce;
    }

    public static PizzaBuilder builder(CheeseType cheeseType, boolean doubleCheese) {
        if (Objects.isNull(cheeseType))
            throw new IllegalArgumentException("cheese required!");

        return new PizzaBuilder(cheeseType, doubleCheese);
    }

    public MeatType getMeatType() {
        return meatType;
    }

    public int getMeatCnt() {
        return meatCnt;
    }

    public CheeseType getCheeseType() {
        return cheeseType;
    }

    public int getCheeseCnt() {
        return cheeseCnt;
    }

    public MashroomsType getMashroomsType() {
        return mashroomsType;
    }

    public boolean isTomato() {
        return tomato;
    }

    public boolean isSauce() {
        return sauce;
    }

    @Override
    public String toString() {
        return "Current Pizza:\n" +
                "\ndough: " + dough +
                "\nmeatCnt: " + meatCnt +
                "\nmeatType: " + meatType +
                "\ncheeseCnt: " + cheeseCnt +
                "\ncheeseType: " + cheeseType +
                "\nmashroomsType: " + mashroomsType +
                "\ntomato: " + tomato +
                "\nsauce: " + sauce;
    }

    public static class PizzaBuilder {
        private MeatType meatType;
        private int meatCnt;
        private CheeseType cheeseType;
        private int cheeseCnt;
        private MashroomsType mashroomsType;
        private boolean tomato;
        private boolean sauce;

        private PizzaBuilder(CheeseType cheeseType, boolean cheeseDouble) {
            this.cheeseType = cheeseType;
            this.cheeseCnt = cheeseDouble ? 2:1;
        }

        public Pizza build() {
            return new Pizza(
                    this.meatCnt,
                    this.meatType,
                    this.cheeseCnt,
                    this.cheeseType,
                    this.mashroomsType,
                    this.tomato,
                    this.sauce
            );
        }

        public PizzaBuilder meat(MeatType meatType, boolean meatDouble) {
            this.meatType = meatType;
            this.meatCnt = meatDouble ? 2:1;

            return this;
        }

        public PizzaBuilder mashrooms(MashroomsType mashroomsType) {
            this.mashroomsType = mashroomsType;

            return this;
        }

        public PizzaBuilder tomato() {
            this.tomato = true;

            return this;
        }

        public PizzaBuilder sauce() {
            this.sauce = true;

            return this;
        }
    }

    public enum MeatType {
        CHIKEN,
        PORK
    }

    public enum MashroomsType {
        AGARICUS, // Шампіьйони
        OYSTER // Вешенки
    }

    public enum CheeseType {
        MOZARELLA, SULGUNI, DORBLU
    }
}
