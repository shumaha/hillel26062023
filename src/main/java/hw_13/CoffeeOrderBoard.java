package hw_13;

import java.util.*;

public class CoffeeOrderBoard {

    private int lastOrderNumber = 0;
    private final LinkedHashMap<Integer, Order> orders;

    public CoffeeOrderBoard() {
            orders = new LinkedHashMap<>();
    }

    public Order add(String customerName) {
        Order order = new Order(customerName, ++lastOrderNumber);
        orders.put(order.getOrderNumber(), order);

        return order;
    }

    public Order deliver() {
        Order order = null;

        Iterator<Map.Entry<Integer, Order>> iterator = orders.entrySet().iterator();
        if (iterator.hasNext()) {
            order = iterator.next().getValue();
            iterator.remove();
        }

        return order;
    }

    public Order deliver(int orderNumber) {

        return orders.remove(orderNumber);
    }

    public void draw() {
        System.out.println("Num\t|\tName");
        for (Map.Entry<Integer, Order> entry: orders.entrySet()) {
            System.out.println(entry.getKey()+"\t|\t" + entry.getValue().getCustomerName());
        }
    }
}
