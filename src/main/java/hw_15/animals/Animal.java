package hw_15.animals;

public abstract class Animal {

    private final int legsCount;

    public Animal(int legsCount) {
        this.legsCount = legsCount;
    }

    public int getLegsCount() {
        return legsCount;
    }
}
