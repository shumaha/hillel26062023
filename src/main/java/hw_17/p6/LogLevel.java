package hw_17.p6;

public enum LogLevel {

    INFO(1),
    DEBUG(2);

    public final int value;

    LogLevel(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return name()+"("+value+")";
    }
}
