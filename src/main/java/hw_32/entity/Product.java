package hw_32.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Product {
    private final int id;
    private final String name;
    private final BigDecimal cost;
}
