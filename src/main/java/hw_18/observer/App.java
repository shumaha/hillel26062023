package hw_18.observer;

import java.time.LocalDateTime;

public class App {
    public static void main(String[] args) {
        AirAlertMessengerBot airAlertBot = new AirAlertMessengerBot();
        MessengerAccount account1 = new MessengerAccount();
        MessengerAccount account2 = new MessengerAccount();
        MessengerAccount account3 = new MessengerAccount();
        
        airAlertBot.addObserver(account1);
        airAlertBot.addObserver(account3);
        
        airAlertBot.notifyObservers(
                new AlarmEvent(true, LocalDateTime.now(), AlarmEvent.RegionId.KYIVSKA, AlarmEvent.RegionId.DNIPROPETROVSKA)
        );

        airAlertBot.addObserver(account2);
        
        airAlertBot.notifyObservers(
                new AlarmEvent(false, LocalDateTime.now(), AlarmEvent.RegionId.KYIVSKA)
        );

        airAlertBot.deleteObserver(account3);

        airAlertBot.notifyObservers(
                new AlarmEvent(false, LocalDateTime.now(), AlarmEvent.RegionId.DNIPROPETROVSKA)
        );
    }

}
