package hw_11.p1;

import java.util.*;

import static java.util.Objects.isNull;

public class Worker {

    public static int countOccurance(List<String> strings, String strToCnt)  {
        int cnt = 0;
        if (isNull(strings))
            return 0;

        for (String s: strings) {
            if (strToCnt.equals(s))
                cnt++;
        }

        return cnt;
    }

    public static <T> List<T> convert(T... arrayToConvert) {
        if (isNull(arrayToConvert))
            throw new IllegalArgumentException("Input array can't be NULL!");

        return Arrays.asList(arrayToConvert);
    }

    public static <T> List<T> findUnique(List<T> listToUnique) {
        if (isNull(listToUnique))
            throw new IllegalArgumentException("Input list can't be NULL!");

        Set<T> uniqueSet = new HashSet<>(listToUnique);

        return new ArrayList<>(uniqueSet);
    }

    public static void calcOccurance(List<String> strings) {
        if (isNull(strings))
            throw new IllegalArgumentException("Input list can't be NULL!");

        Map<String, Integer> countMap = new HashMap<>();
        for (String s : strings) {
            Integer cnt = countMap.get(s);
            if (isNull(cnt))
                countMap.put(s, 1);
            else
                countMap.put(s, ++cnt);
        }

        for (Map.Entry<String, Integer> entry: countMap.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }

    public static List<Occurance> findOccurance(List<String> strings) {
        if (isNull(strings))
            throw new IllegalArgumentException("Input list can't be NULL!");

        Map<String, Integer> countMap = new HashMap<>();
        for (String s : strings) {
            Integer cnt = countMap.get(s);
            if (isNull(cnt))
                countMap.put(s, 1);
            else
                countMap.put(s, ++cnt);
        }

        List<Occurance> occurances = new ArrayList<>();
        for (Map.Entry<String, Integer> entry: countMap.entrySet()) {
            occurances.add(new Occurance(entry.getKey(), entry.getValue()));
        }

        return occurances;
    }
}
