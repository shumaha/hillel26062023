package hw_22;

import java.util.AbstractQueue;

public class GasStation {
    private final StationFuelStorage stationFuelStorage;
    private final int countGasUnit;

    public GasStation(int maxCapacity, int countGasUnit) {
        this.stationFuelStorage = new StationFuelStorage((int) (maxCapacity*0.3),maxCapacity, maxCapacity);

        if (countGasUnit < 2)
            throw new IllegalArgumentException("Count GasUnit can't be < 2");

        this.countGasUnit = countGasUnit;
    }

    public void startWork(AbstractQueue<Car> carQueue) {
        System.out.println("GasStation initail capacity: " + stationFuelStorage.getCurrentCapacity() + ", minCapacity: " + stationFuelStorage.getMinCapacity());
        System.out.println();

        for (int i = 0; i < countGasUnit; i++) {
            Thread threadGasUnit = new Thread(new GasStationUnit(carQueue), "Thread GasUnit " + (i+1));
            threadGasUnit.start();
        }

        Thread threadStorageFillUpWorker = new Thread(new StorageFillUpWorker(), "Thread Storage Fill-Up Worker");
        threadStorageFillUpWorker.start();
    }


    private class GasStationUnit implements Runnable {
        private final AbstractQueue<Car> carQueue;

        private GasStationUnit(AbstractQueue<Car> carQueue) {
            this.carQueue = carQueue;
        }

        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                stationFuelStorage.ifMinThenWait();

                if ((stationFuelStorage.getCurrentCapacity()) > stationFuelStorage.getMinCapacity()) {
                    if (!carQueue.isEmpty()) {
                        Car car = carQueue.remove(); // next car
                        serveCar(car);
                    } else { // emty queue - waiting
                        System.out.println(Thread.currentThread().getName()+" Queue is empty - waiting...");
                        try {
                            Thread.sleep( 10000);
                        } catch (InterruptedException ignored) {}
                    }
                }
            }
        }

        private void serveCar(Car car) {
            String servedMessage = "Served car: " + car;

            try {
                Thread.sleep( 1000);
            } catch (InterruptedException ignored) {}

            FuelStorage carFS = car.getFuelStorage();

            int litresNeed = carFS.getLitresToMax();
            int litresGot = 0;
            try {
                litresGot = stationFuelStorage.take(litresNeed);
                carFS.put(litresGot);
            } catch (FuelStorageOverPutException | FuelStorageEmptyException ignored) {}

            servedMessage += ", needed/filled up "+litresNeed+"/"+litresGot+" l. StationStorage capacity: " + stationFuelStorage.getCurrentCapacity();

            System.out.println(Thread.currentThread().getName() +" "+ servedMessage);
        }
    }

    private class StorageFillUpWorker implements Runnable {
        @Override
        public void run() {
            while (true) {
                stationFuelStorage.ifNonMinThenWait();

                if (stationFuelStorage.getCurrentCapacity() <= stationFuelStorage.getMinCapacity()) {
                    try {
                        try {
                            Thread.sleep( 5000);
                        } catch (InterruptedException ignored) {}
                        stationFuelStorage.put(stationFuelStorage.getLitresToMax());
                        System.out.println("------------ GasStation capacity after filling up: " + stationFuelStorage.getCurrentCapacity()+" ------------");
                    } catch (FuelStorageOverPutException ignored) {}
                }
            }
        }
    }
}
