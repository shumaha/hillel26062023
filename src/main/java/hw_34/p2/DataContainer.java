package hw_34.p2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class DataContainer {

    private final List<Long> storage = new ArrayList<>();
    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    private final Lock writeLock = lock.writeLock();
    private final Lock readLock = lock.readLock();

    public void readData() {
        readLock.lock();
        try {
            long num = storage.isEmpty() ? -1 : storage.get(storage.size()-1);
            System.out.println("Get(): " + num);
        } finally {
            readLock.unlock();
        }
    }

    public void writeData(long num) {
        writeLock.lock();
        try {
            storage.add(num);
            System.out.println("Add(): " + num);
        } finally {
            writeLock.unlock();
        }
    }

    public static void sleep(long timeout) {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
