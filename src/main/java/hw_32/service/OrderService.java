package hw_32.service;

import hw_32.dto.OrderCreateDto;
import hw_32.entity.Order;
import hw_32.entity.Product;
import hw_32.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Objects.isNull;

@Component
public class OrderService {

    @Autowired
    OrderRepository orderRepository;
    @Autowired
    ProductService productService;
    public List<Order> showAll() {
        Optional<List<Order>> orders = orderRepository.selectAll();

        return orders.orElse(null);

    }

    public Order findOrderById(int id) {
        Optional<Order> optionalOrder = orderRepository.findOrderById(id);

        return optionalOrder.orElse(null);
    }

    public Order addOrder(String date, List<Integer> products) {

        List<Integer> uniqueIds = products.stream()
                .distinct()
                .toList();

        Map<Integer, BigDecimal> productCosts = new HashMap<>();

        for (Integer i: uniqueIds) {
            Product product = productService.findProductById(i);

            if (isNull(product)) {
                return null;
            }

            productCosts.put(i, product.getCost());
        }

        BigDecimal orderCost = new BigDecimal("0.0");

        for (int i: products) {
            orderCost = orderCost.add(productCosts.get(i));
        }

        return orderRepository.addOrder(date, orderCost, products).orElse(null);

    }

}
