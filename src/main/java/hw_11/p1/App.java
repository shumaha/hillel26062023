package hw_11.p1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class App {
    public static void main(String[] args) {
//        test_1();
//        test_2();
//        test_3();
//        test_4_1();
        test_4_2();
    }

    // 1. Створити метод countOccurance,
    public static void test_1() {
        List<String> myList = new ArrayList<>();
        myList.addAll(Arrays.asList("one", "two", "three", "one", "four"));
        String wordToCnt = "one";
        System.out.println("Cnt of word ['"+wordToCnt+"']: " + Worker.countOccurance(myList, wordToCnt));
    }

    // 2. Створити метод toList
    public static void test_2() {
        String[] array = {"one", "two", "three", "one", "four"};
        List<String> myList2 = Worker.convert(array);
        System.out.println("Converted " + myList2.getClass().getSimpleName() + ": " + myList2);
    }

    // 3. Створити метод findUnique
    public static void test_3() {
        List<Integer> myList = new ArrayList<>(Arrays.asList(8,1,2,4,6,3,6,5,5,2,8,6,3));
        System.out.println("findUnique(): " + Worker.findUnique(myList));
    }

    //4 ** Створити метод calcOccurance
    public static void test_4_1() {
        List<String> myList = Arrays.asList("apple", "orange", "banana", "apple", "orange", "apple");
        Worker.calcOccurance(myList);
    }

    //4 *** Створити метод findOccurance
    public static void test_4_2() {
        List<String> myList = Arrays.asList("apple", "orange", "banana", "apple", "orange", "apple");
        System.out.println("Words: " + myList);
        System.out.println(Worker.findOccurance(myList));
    }
}
