package hw_24.logging.common;

public abstract class LoggerConfiguration {
    public static final String DEFAULT_MESSAGE_FORMAT = "{className} {level} {time} {message}";
    public static final String DEFAULT_DATE_FORMAT = "yyyy.MM.dd hh:mm:ss";
    public static final LogLevel DEFAULT_LEVEL = LogLevel.INFO;
    private final String currentMessageFormat;
    private final String currentDateFormat;
    private final LogLevel level;

    public LoggerConfiguration(String currentMessageFormat, String currentDateFormat, LogLevel level) {
        this.currentMessageFormat = currentMessageFormat;
        this.currentDateFormat = currentDateFormat;
        this.level = level;
    }

    public String getCurrentMessageFormat() {
        return currentMessageFormat;
    }

    public String getCurrentDateFormat() {
        return currentDateFormat;
    }

    public LogLevel getLevel() {
        return level;
    }
}
