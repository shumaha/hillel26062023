package hw_31;

import hw_31.entity.Product;
import hw_31.repository.CartRepository;
import hw_31.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Scanner;
import java.util.UUID;

@Component
public class CartController {
    private final ApplicationContext applicationContext;
    private final Scanner scanner = new Scanner(System.in);
    private final ProductRepository productRepository;
    @Autowired
    private CartRepository cartRepository;

    public CartController(ApplicationContext applicationContext, ProductRepository productRepository) {
        this.applicationContext = applicationContext;
        this.productRepository = productRepository;
    }

    public void start() {
        System.out.println("Input command (type help for information):");
        String command = scanner.nextLine();
        while (!command.equals("exit")) {
            switch (command) {
                case "new" -> cartRepository = applicationContext.getBean(CartRepository.class);
                case "add" -> addToCart();
                case "delete" -> deleteFromCart();
                case "print" -> System.out.println(printCart());
                case "catalog" -> System.out.print(printCatalog());
                case "help" -> System.out.println(printHelp());

                default -> System.out.println("Command is not valid. Please repeat");
            }

            System.out.println("\nInput command (type help for information):");
            command = scanner.nextLine();
        }
    }

    private String printHelp() {

        return "Available commands: \n" +
                "help - show help\n" +
                "exit - exit from program\n" +
                "catalog - show all available products\n" +
                "new - create new cart\n" +
                "add - add product to cart by UUID\n" +
                "delete - delete product from cart by UUID\n" +
                "print - print content of cart";
    }

    private String printCatalog() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Product product: productRepository.findAll()) {
            stringBuilder.append(product).append("\n");
        }

        return stringBuilder.toString();
    }

    private String printCart() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Content of Cart: {").append("\n");
        for (Product product: cartRepository.findAll()) {
            stringBuilder.append("\t").append(product).append("\n");
        }
        stringBuilder.append("}");

        return stringBuilder.toString();
    }

    private void addToCart() {
        System.out.println("Input UUID product:");
        UUID uuid = UUID.fromString(scanner.nextLine());

        if (cartRepository.add(uuid)) {
            System.out.println("Adding is successfull");
        } else {
            System.out.println("Adding is failed");
        }
    }

    private void deleteFromCart() {
        System.out.println("Input UUID product:");
        UUID uuid = UUID.fromString(scanner.nextLine());

        if (cartRepository.delete(uuid)) {
            System.out.println("Deleting is successfull");
        } else {
            System.out.println("Deleting is failed");
        }
    }
}
