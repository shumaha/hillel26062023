package hw_19.abstractFactory.factories;

public class YoutubeVideo extends Video {

    protected YoutubeVideo() {
        super("Youtube Video");
    }
}
