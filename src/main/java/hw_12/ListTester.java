package hw_12;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class ListTester {

    public static void test() {
//        testAdd1();
//        testAdd2();
//        testAddAll_1();
//        testAddAll_2();
//        testGet();
//        testSet();
//        testIterator();
//        testSizeAndIsEmpty();
//        testContains();
//        testContainsAll();
//        testToArray1();
//        testToArray2();
//        testRemove_1();
//        testRemove_2();
        testRemoveAll();
//        testRetainAll();
//        testClear();
//        testIndexOf();
//        testLastIndexOf();
    }

    private static void testAdd1() {
        List<String> myList = new MyLinkedList();
        System.out.println("before" + myList);
        for (int i = 0; i < 20; i++) {
            myList.add(Integer.toString(i));
        }

        System.out.println("after" + myList); // toString()
    }

    private static void testAdd2() {
        MyLinkedList<String> myList = generateMyLinkedList(20);
        System.out.println("before" + myList);
        myList.add(0, "55");
        System.out.println("after" + myList);
    }

    private static void testAddAll_1() {
        MyLinkedList<String> myList = generateMyLinkedList(20);
        System.out.println("before " + myList);

        MyLinkedList<String> myListToAdd = new MyLinkedList<>();
        for (int i = 20; i <= 50; i++) {
            myListToAdd.add(Integer.toString(i));
        }
        System.out.println("myListToAdd: " + myListToAdd);
        boolean result = myList.addAll(myListToAdd);
        System.out.println("after " + myList);
        System.out.println("Result: " + result);
    }

    private static void testAddAll_2() {
        MyLinkedList<String> myList = generateMyLinkedList(20);
        System.out.println("before " + myList);

        MyLinkedList<String> myListToAdd = new MyLinkedList<>();
        for (int i = 90; i < 92; i++) {
            myListToAdd.add(Integer.toString(i));
        }
        boolean result = myList.addAll(0, myListToAdd);
        System.out.println("after " + myList);
        System.out.println("Result: " + result);
    }

    private static void testRemove_1() {
        MyLinkedList<String> myList = generateMyLinkedList(20);
        System.out.println("before " + myList);

        int indexToRemove = 19;
        String removedElement = myList.remove(indexToRemove);
        System.out.println("after " + myList);
        System.out.println("Removed element["+indexToRemove+"]: " + removedElement);
    }

    private static void testRemove_2() {
        MyLinkedList<String> myList = generateMyLinkedList(20);
        System.out.println("before " + myList);

        String elementToRemove = "19";
        boolean result = myList.remove(elementToRemove);
        System.out.println("after " + myList);
        System.out.println("Removed element["+elementToRemove+"]: " + result);
    }

    private static void testRemoveAll() {
        MyLinkedList<String> myList = generateMyLinkedList(5);
        myList.add("1");
        myList.add(null);
        myList.add("2");
        System.out.println("before " + myList);

        MyLinkedList<String> elementsToRemove = new MyLinkedList<>();
        elementsToRemove.add("2");
        elementsToRemove.add("1");
        elementsToRemove.add(null);

        boolean result = myList.removeAll(elementsToRemove);
        System.out.println("after " + myList);
        System.out.println("Removed element["+elementsToRemove+"]: " + result);
    }

    private static void testRetainAll() {
        MyLinkedList<String> myList = generateMyLinkedList(5);
        myList.add("1");
        myList.add("2");
        System.out.println("before " + myList);

        MyLinkedList<String> elementsToRetain = new MyLinkedList<>();
        elementsToRetain.add("0");
        elementsToRetain.add("2");
        elementsToRetain.add("3");
        boolean result = myList.retainAll(elementsToRetain);
        System.out.println("after " + myList);
        System.out.println("elementsToRetain: " + elementsToRetain + ", result: " + result);
    }

    private static void testClear() {
        MyLinkedList<String> myList = generateMyLinkedList(10);
        System.out.println("before " + myList);
        myList.clear();
        System.out.println("after " + myList);
    }

    private static void testGet() {
        MyLinkedList<String> myList = generateMyLinkedList(10);
        System.out.println(myList);
        int index = 6;
        System.out.println("Element["+index+"], value=" + myList.get(index));
    }

    private static void testSet() {
        MyLinkedList<String> myList = generateMyLinkedList(10);
        System.out.println("before " + myList);
        int index = 5;
        String prev = myList.set(index, "55");
        System.out.println("after " + myList);
        System.out.println("Previous element["+index+"], value=" + prev);
    }

    private static void testIndexOf() {
        MyLinkedList<String> myList = generateMyLinkedList(10);
        String s = "999";
        int index = 7;
        myList.add(index, s);
        System.out.println(myList);
        System.out.println("Element["+s+"], setted index["+index+"], founded index=" + myList.indexOf(s));
    }

    private static void testLastIndexOf() {
        MyLinkedList<String> myList = generateMyLinkedList(10);
        String s = "3";
        myList.add(s);
        System.out.println(myList);
        System.out.println("Element["+s+"], founded last index=" + myList.lastIndexOf(s));
    }

    private static void testIterator() {
        MyLinkedList<String> myList = generateMyLinkedList(20);
        Iterator<String> iterator = myList.iterator();
        while (iterator.hasNext())
            System.out.print(iterator.next() + " ");
    }

    private static void testSizeAndIsEmpty() {
        MyLinkedList<String> myList = generateMyLinkedList(20);
        System.out.println(myList + ", size(): " + myList.size() + ", isEmpty: " + myList.isEmpty());
        MyLinkedList<String> myEmptyList = new MyLinkedList<>();
        System.out.println(myEmptyList + ", size(): " + myEmptyList.size() + ", isEmpty: " + myEmptyList.isEmpty());
    }

    private static void testContains() {
        MyLinkedList<String> myList = generateMyLinkedList(20);
        String forContains = "6";
        System.out.println(myList + ", contains("+forContains+"): " + myList.contains(forContains));
        forContains = "99";
        System.out.println(myList + ", contains("+forContains+"): " + myList.contains(forContains));
    }

    private static void testContainsAll() {
        MyLinkedList<String> myList = generateMyLinkedList(20);
        System.out.println(myList);

        MyLinkedList<String> elementsToContains = new MyLinkedList<>();
        elementsToContains.add("1");
        elementsToContains.add("2");
        elementsToContains.add("99");
        boolean result = myList.containsAll(elementsToContains);
        System.out.println("Contains elements["+elementsToContains+"]: " + result);
    }

    private static void testToArray1() {
        MyLinkedList<String> myList = generateMyLinkedList(20);
        Object[] newObj = myList.toArray();
        System.out.println(myList);
        System.out.println("Object[]: " + Arrays.toString(newObj));
    }

    private static void testToArray2() {
        MyLinkedList<String> myList = generateMyLinkedList(10);

        // array 1 test (size < list)
        String[] arrayLess = new String[5];
        arrayLess[2] = "end";
        arrayLess[1] = "end2";
        System.out.println(myList);
        System.out.println("\narrayLess before [size("+arrayLess.length+")]: " + Arrays.toString(arrayLess));
        String[] array2 = myList.toArray(arrayLess);
        System.out.println("arrayLess after [size("+array2.length+")]: " + Arrays.toString(array2));

        // array 2 test (size >= list)
        String[] arrayGreater = new String[myList.size()+2];
        arrayGreater[arrayGreater.length-2]="pre_last_element";
        arrayGreater[arrayGreater.length-1]="last_element";
        System.out.println("\narrayGreater before [size("+arrayGreater.length+")]: " + Arrays.toString(arrayGreater));
        arrayGreater = myList.toArray(arrayGreater);
        System.out.println("arrayGreater after [size("+arrayGreater.length+")]: " + Arrays.toString(arrayGreater));
    }

    private static MyLinkedList<String> generateMyLinkedList(int cnt) {
        MyLinkedList<String> myList = new MyLinkedList<>();

        for (int i = 0; i < cnt; i++) {
            myList.add(Integer.toString(i));
        }

        return myList;
    }
}
