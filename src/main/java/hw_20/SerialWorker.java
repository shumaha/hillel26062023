package hw_20;

import java.io.*;

public class SerialWorker<T> {

    public boolean saveObject(T object, String filePath) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filePath))) {
            oos.writeObject(object);
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }

        return true;
    }

    @SuppressWarnings("unchecked")
    public T restoreObject(String filePath) {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filePath))) {
            return (T) ois.readObject();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
