package hw_17.p6;

public class FileLoggerConfiguration {
    private final String fileName;
    private final LogLevel level;
    private final long maxSize;
    private final String format;

    public FileLoggerConfiguration(String fileName, LogLevel level, long maxSize, String format) {
        this.fileName = fileName;
        this.level = level;
        this.maxSize = maxSize;
        this.format = format;
    }

    public String getFileName() {
        return fileName;
    }

    public LogLevel getLevel() {
        return level;
    }

    public long getMaxSize() {
        return maxSize;
    }

    public String getFormat() {
        return format;
    }
}
