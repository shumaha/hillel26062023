package hw_21;

public class FileCountCharsHandler extends FileHandler implements Runnable {
    public FileCountCharsHandler(String pathFileToRead, String pathFileToWrite) {
        super(pathFileToRead, pathFileToWrite);
    }

    @Override
    public String handleFileContent() {

        return "Count of chars: " + getFileContent().length();
    }


    @Override
    public void run() {
        writeResult(handleFileContent());
    }
}
