package hw_34.p2;

import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicLong;

public class App {
    public static void main(String[] args) {
        DataContainer container = new DataContainer();


        AtomicLong readCount = new AtomicLong(0);
        AtomicLong writeCount = new AtomicLong(0);

        int testTime = 10;

        Runnable runnable_read = () -> {
            LocalDateTime dt_future = LocalDateTime.now().plusSeconds(testTime);
            while (LocalDateTime.now().isBefore(dt_future)) {
                container.readData();
                readCount.addAndGet(1L);
                DataContainer.sleep(10);
            }
            System.out.println("Read count: " + readCount.get());
        };

        Runnable runnable_write = () -> {
            LocalDateTime dt_future = LocalDateTime.now().plusSeconds(testTime);
            while (LocalDateTime.now().isBefore(dt_future)) {
                writeCount.addAndGet(1L);
                container.writeData(writeCount.get());
                DataContainer.sleep(10);
            }
            System.out.println("Write count: " + writeCount.get());
        };

        Thread reader1 = new Thread(runnable_read);
        Thread reader2 = new Thread(runnable_read);
        Thread reader3 = new Thread(runnable_read);
        Thread reader4 = new Thread(runnable_read);

        Thread writer1 = new Thread(runnable_write);
        Thread writer2 = new Thread(runnable_write);

        reader1.start();
        reader2.start();
        reader3.start();
        reader4.start();
        writer1.start();
        writer2.start();
    }
}
