package hw_18.decorator.decorators;

import hw_18.decorator.interfaces.SQLRequestFormatter;

public abstract class SQLRequestFormatterDecorator implements SQLRequestFormatter {

    private final SQLRequestFormatter sqlRequestFormatter;

    public SQLRequestFormatterDecorator(SQLRequestFormatter sqlRequestFormatter) {
        this.sqlRequestFormatter = sqlRequestFormatter;
    }

    abstract String decorate(String sqlRequest);

    @Override
    public String format(String sqlRequest) {
        String decorated = decorate(sqlRequest);

        return sqlRequestFormatter.format(decorated);
    }
}
