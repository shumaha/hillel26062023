package hw_32.repository;

import hw_32.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class ProductRepository {

    @Autowired
    private DataBaseConnection dataBaseConnection;

    private static final String FIND_PRODUCT_BY_ID =
            """
            SELECT id, name, cost
            FROM Product
            where id = ?;
            """;
    private static final String FIND_ALL_PRODUCTS =
            """
            SELECT id, name, cost
            FROM Product;
            """;
    private static final String INSERT_PRODUCT = "INSERT INTO Product(name, cost) VALUES(?, ?);";

    public List<Product> selectAll() {
        List<Product> products = new ArrayList<>();

        try {
            Connection connection = dataBaseConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_PRODUCTS);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                BigDecimal cost = BigDecimal.valueOf(resultSet.getFloat("cost"));

                products.add(new Product(id, name, cost));
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Exception occurred when tries to get products!");
        }

        return products;
    }

    public Optional<Product> findProductById(int idToFind) {

        try {
            Connection connection = dataBaseConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_PRODUCT_BY_ID);
            preparedStatement.setInt(1, idToFind);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                BigDecimal cost = BigDecimal.valueOf(resultSet.getFloat("cost"));

                return Optional.of(new Product(id, name, cost));
            }

            return Optional.empty();

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Exception occurred when tries to find product by ID!");
        }
    }

}

