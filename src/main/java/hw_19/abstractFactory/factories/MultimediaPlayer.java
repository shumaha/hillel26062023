package hw_19.abstractFactory.factories;

public class MultimediaPlayer {

    private final MultimediaFactory multimediaFactory;

    public MultimediaPlayer(MultimediaFactory videoFactory) {
        this.multimediaFactory = videoFactory;
    }

    public Video getRandomVideo() {
        return multimediaFactory.generateVideo();
    }

    public Music getRandomMusic() {
        return multimediaFactory.generateMusic();
    }


}
