package hw_19.templateMethod.games.TikTakToe;

public class OutbundInputException extends Exception {
    public OutbundInputException(String message) {
        super(message);
    }
}
