package hw_7;

public class App {
    public static void main(String[] args) {
        String[][] myMatrix2 = new String[][] {
                {"1","2","3","4"},
                {"5","6","7","8"},
                {"9","10","11","12"},
                {"13","14","15","16"}
        };
        String[][] myMatrix3 = new String[][]{
                {"1","2", "3"},
                {"4","5","6"}
        };

        String[][] myMatrix4 = new String[][]{
                {"1","A"},
                {"3","4"}
        };

        ArrayValueCalculator arrayValueCalculator = new ArrayValueCalculator();


        int matrixSum = -1; // -1 = fail result
        try {
            matrixSum = arrayValueCalculator.doCalc(myMatrix2);
//            matrixSum = arrayValueCalculator.doCalc(myMatrix3); // invalid matrix
//            matrixSum = arrayValueCalculator.doCalc(myMatrix4); // invalid matrix

        } catch (ArraySizeException e) {
            e.printStackTrace();
            System.out.println("Проблема з розміром матриці: " + e);
        } catch (ArrayDataException e) {
            e.printStackTrace();
            System.out.println("Проблема з елементами матриці: " + e);
        }

        System.out.println("Sum of matrix: " + matrixSum);
    }
}
