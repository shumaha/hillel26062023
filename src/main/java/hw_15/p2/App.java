package hw_15.p2;

import hw_15.animals.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {

    public static void main(String[] args) {
        List<Animal> animals = getRandomAnimalList(100);

//        System.out.println("Total number of legs: " + getTotalLegs(animals));
//        groupByNumberLegs(animals);
//        countBySpecies(animals);
        countOfSpecies(animals);
    }

    // Q.4) Take a list of 100 random animals
    public static List<Animal> getRandomAnimalList(int count) {
        String[] speciesList = new String[]{"Cat", "Dog", "Lion", "Monkey", "Snake"};

        List<Animal> animals = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            int randomIndex = (int) (Math.random() * (speciesList.length));
            animals.add(generateAnimal("hw_15.animals."+speciesList[randomIndex]));
        }

        return animals;
    }

    // Q.5) Find the total number of legs
    public static int getTotalLegs(List<Animal> list) {

        return list.stream().mapToInt(Animal::getLegsCount).sum();
    }


    // Q.6) Group the animals by their number of legs
    public static void groupByNumberLegs(List<Animal> list) {
        Map<Integer, List<Animal>> collected = list.stream()
                .sorted(Comparator.comparing(s -> s.getClass().getSimpleName()))
                .collect(Collectors.groupingBy(Animal::getLegsCount));


        for (int key: collected.keySet()) {
            System.out.println("Group ("+collected.get(key).size()+" pcs.) of animals with "+key+" legs: " + collected.get(key));
        }
    }

    // Q.7) Count the number of animals in each specie
    public static void countBySpecies(List<Animal> list) {

        Map<String, List<Animal>> collected = list.stream()
                .collect(Collectors.groupingBy(t -> t.getClass().getSimpleName()));

        for (String s: collected.keySet()) {
            System.out.println("Count of " + s + " - " + collected.get(s).size() + " pcs.");
        }
    }

    // Q.8) Count the number of species
    public static void countOfSpecies(List<Animal> list) {

        Map<String, List<Animal>> collected = list.stream()
                .collect(Collectors.groupingBy(t -> t.getClass().getSimpleName()));

        System.out.println("Count of species: " + collected.size());
    }

    public static Animal generateAnimal(String animalClassName) {

        try {
            Constructor<?> constructor = Class.forName(animalClassName).getConstructor();
            Object obj = constructor.newInstance();

            return (Animal) obj;

        } catch (ClassNotFoundException | NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException ex) {
            ex.printStackTrace();
        }

        return null;
    }


}
