package hw_21;

public class FileCountWordsHandler extends FileHandler implements Runnable {
    public FileCountWordsHandler(String pathFileToRead, String pathFileToWrite) {
        super(pathFileToRead, pathFileToWrite);
    }

    @Override
    public String handleFileContent() {
        String[] words = getFileContent().split("\\w+");

        return "Count of words: " + (words.length-1);
    }

    @Override
    public void run() {
        writeResult(handleFileContent());

    }
}
