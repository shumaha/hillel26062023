package hw_6.p1;

public class App {

    public static void main(String[] args) {
        Shape[] shapes = new Shape[]{
                new Triangle(2),
                new Square(13),
                new Circle(10),
                new Triangle(43)
        };

        double areaSum = 0;
        for (Shape shape: shapes) {
            areaSum += shape.getArea();
        }

        System.out.format("Total sum of shapes area: %.2f%n", areaSum);
    }
}
