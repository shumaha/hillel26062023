package hw_14.p1;

public class Product {

    final private Type type;
    private int price;

    public Product(Type type, int price) {
        this.type = type;
        this.price = price;
    }

    public Type getType() {
        return type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public enum Type {
        Book,
        Toy
    }

    @Override
    public String toString() {
        return "Product{" +
                "type=" + type +
                ", price=" + price +
                '}';
    }
}