package hw_6.p2;

import static java.util.Objects.isNull;

/**
 * Клас для виконання змагання
 */
public class Competition {

    /**
     * Головний метод для виклику проведення змагання
     */
    public static void compete(Participant participant, Obstacle... obstacles) {
        if (isNull(participant))
            throw new RuntimeException("Participant can't be null!");
        if (isNull(obstacles) || obstacles.length == 0)
            throw new RuntimeException("Obstacles can't be null or void!");

        CompetitionPrinter printer = new CompetitionPrinter(10); // зменшуємо масштаб у 10 разів

        printer.print(participant.getLabel(), null, obstacles); // start print
        pause();

        int distance = 0;
        Obstacle prevObstacle = null;
        for (Obstacle obstacle : obstacles) {
            distance += obstacle.getlength();

            if (obstacle.overcome(participant) ) {
                printer.print(participant.getLabel(), obstacle, obstacles);
                System.out.println("______________________________________________________________________________________________________");
                System.out.println("Учасник ["+ participant +"] пройшов перешкоду ["+obstacle+"] на дистанції ["+distance+"]");
                prevObstacle = obstacle;
            }
            else {
                printer.print(":(", prevObstacle, obstacles);
                System.out.println("______________________________________________________________________________________________________");
                System.out.println("Учасник [" + participant + "] не пройшов перешкоду [" + obstacle + "] на дистанції [" + distance + "]");
                return;
            }
            pause();
        }
    }

    private static void pause() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
