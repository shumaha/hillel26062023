package hw_28;

import hw_28.entity.Homework;
import hw_28.entity.Lesson;
import hw_28.repository.LessonDao;

import java.util.List;
import java.util.Optional;

public class App {
    public static void main(String[] args) {

        LessonDao lessonDao = new LessonDao();

        // a) метод додавання уроку
        Homework homework = new Homework("HW_29", "підняти MySQL на локальній машині...");
        Lesson lessonToAdd = new Lesson("Lesson 34", "2023-10-20 19:15:00", homework);
        System.out.println("Add result: " + lessonDao.addLesson(lessonToAdd));

        homework = new Homework("HW_30", "1. Створити таблицю Homework. Ця таблиця складається з атрибутів: id, name, description...");
        lessonToAdd = new Lesson("Lesson 35", "2023-10-23 19:15:00", homework);
        System.out.println("Add result: " + lessonDao.addLesson(lessonToAdd));

        homework = new Homework("HW_31", "створити новий Maven проект на гіті...");
        lessonToAdd = new Lesson("Lesson 36", "2023-10-27 19:15:00", homework);
        System.out.println("Add result: " + lessonDao.addLesson(lessonToAdd));

        List<Lesson> lessons = lessonDao.getLessons();
        System.out.println("All lessons: " + lessons);


        // b) метод видалення уроку
        Lesson lessonForDelete = lessons.get(lessons.size()-1);
        boolean result = lessonDao.deleteLesson(lessonForDelete.getId());
        System.out.println("Delete result of deleting lesson "+lessonForDelete.getId()+": " + result);

        // c) метод отримання всіх уроків
        lessons = lessonDao.getLessons();
        System.out.println("All lessons: " + lessons);

        // d) метод отримання уроку за ID
        Lesson lessonToGet = lessons.get(lessons.size()-1);
        Optional<Lesson> lesson = lessonDao.getLessonById(lessonToGet.getId());
        lesson.ifPresent(item -> System.out.println(item));
    }
}
