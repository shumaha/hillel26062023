package hw_24;

import hw_24.logging.common.Logger;
import hw_24.logging.factory.FileLogerFactory;
import hw_24.logging.factory.StdoutLogerFactory;

import java.io.IOException;

public class App {
    private static final Logger FILE_LOGGER = FileLogerFactory.getLogger(App.class);
    private static final Logger STDOUT_LOGGER = StdoutLogerFactory.getLogger(App.class);

    public static void main(String[] args) throws IOException {

        // log messages
        FILE_LOGGER.info("Warning message");
        FILE_LOGGER.debug("Error message");
        FILE_LOGGER.debug("Error message 2");

        STDOUT_LOGGER.info("Warning message");
        STDOUT_LOGGER.debug("Error message");
        STDOUT_LOGGER.debug("Error message 2");
    }
}
