package hw_4;

public class Dog implements Animal {
    private String name;

    public Dog(String name) {
        this.name = name;
    }
    @Override
    public void run(int meters) {
        System.out.println(name + " пробіг " + (meters > 500 ? 500:meters) + " з потрібних " + meters + " м.");
    }

    @Override
    public void swim(int meters) {
        System.out.println(name + " проплив " + (meters > 10 ? 10:meters) + " з потрібних " + meters + " м.");
    }
}
