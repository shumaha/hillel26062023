package hw_33.service;

import hw_33.entity.Product;
import hw_33.repository.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

@Component
@AllArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;


    public List<Product> showAll() {

        return StreamSupport.stream(productRepository.findAll().spliterator(), false).toList();
    }

    public Product findProductById(long id) {
        Optional<Product> optionalProduct = productRepository.findById(id);

        return optionalProduct.orElse(null);


    }

}
