package hw_17.p6;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static java.util.Objects.isNull;

public class FileLogger {
    private final FileLoggerConfiguration conf;
    private OutputStream outputStream;
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private final DateTimeFormatter newFileNameformatter = DateTimeFormatter.ofPattern("dd.MM.yyyy-HH.mm");
    private long writedBytesCount;

    public FileLogger(FileLoggerConfiguration conf) {
        this.conf = conf;
        File file = new File(conf.getFileName());
        writedBytesCount = file.length();

        try {
            outputStream = new BufferedOutputStream(
                    new FileOutputStream(file, true), 50);
        }
        catch (IOException e) {
            throw new RuntimeException("Can't create stream for writing log to file ("+file+")!");
        }
    }

    public void info(String message) {
        if (canLog(LogLevel.INFO)) {
            writeMessage(templateMessage(message, LogLevel.INFO));
        }
    }

    public void debug(String message) {
        if (canLog(LogLevel.DEBUG)) {
            writeMessage(templateMessage(message, LogLevel.DEBUG));
        }
    }

    private void writeMessage(String message) {
        byte[] bytes = message.getBytes(StandardCharsets.UTF_8);

        if ( (writedBytesCount+bytes.length) >= conf.getMaxSize()) {
            outputStream = getNewOutputStream(bytes.length);
        }

        writedBytesCount += bytes.length;

        try {
            outputStream.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean canLog(LogLevel level) {

        return level.value >= conf.getLevel().value;
    }

    private String templateMessage(String message, LogLevel level) {
        String templetedStr = conf.getFormat();
        templetedStr = templetedStr.replace("[CURRENT_TIME]", LocalDateTime.now().format(formatter));
        templetedStr = templetedStr.replace("[LEVEL]", level.name());
        templetedStr = templetedStr.replace("[MESSAGE_STRING]", message);

        return templetedStr + "\n";
    }

    public void close() {
        try {
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File getNewFile(String fullFileName, int countToWrite) {

        int lastIndex = fullFileName.lastIndexOf('\\');
        String dirName = fullFileName.substring(0, lastIndex+1);
        if (dirName.isEmpty())
            dirName = ".\\";

        String fileName = fullFileName.substring(lastIndex+1);
        lastIndex = fileName.lastIndexOf('.');
        String firstPartName = fileName.substring(0, lastIndex);
        String fileExtension = fileName.substring(lastIndex);

        String newFileName = firstPartName + "_" + LocalDateTime.now().format(newFileNameformatter);

        String postFix = ""; int id = 1;
        while (true) {
            String fileNameToTest = newFileName + postFix + fileExtension;
            File newFile = checkNewFileName(dirName, fileNameToTest);

            if (isNull(newFile)) {
                return new File(dirName, fileNameToTest);
            }
            // validate max size of file
            else if (newFile.length()+countToWrite < conf.getMaxSize()) {
                return newFile;
            }

            postFix = "_"+id++;
        }
    }

    private OutputStream getNewOutputStream(int countToWrite) {

        File file = getNewFile(conf.getFileName(), countToWrite);
        writedBytesCount = file.length();

        try {
            return
                    new BufferedOutputStream(
                            new FileOutputStream(file, true), 50);
        }
        catch (IOException e) {
            throw new RuntimeException("Can't create stream for writing log to file ("+file+")!");
        }
    }

    public File checkNewFileName(String dirPath, String fileNameToCheck) {
        File dir = new File(dirPath);
        FilenameFilter filter = (d, name) -> name.equals(fileNameToCheck);
        String[] list = dir.list(filter);

        if (list.length > 0) {
            return new File(dirPath, fileNameToCheck);
        }

        return null;
    }
}
