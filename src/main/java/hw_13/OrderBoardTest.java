package hw_13;

public class OrderBoardTest {
    public static void test(CoffeeOrderBoard orderBoard) {
        orderBoard.add("John Travolta");
        orderBoard.add("Keanu Reeves");
        orderBoard.add("Arnold Schwarzenegger");
        orderBoard.add("Will Smith");
        orderBoard.draw();

        // last delivered
        System.out.println("\ndelivered last: " + orderBoard.deliver());

        // delivered by order number
        int orderNumberToDeliver = 3;
        System.out.println("delivered by order number("+orderNumberToDeliver+"): " + orderBoard.deliver(orderNumberToDeliver));

        System.out.println();
        orderBoard.draw();
    }
}
