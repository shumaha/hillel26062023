package hw_6.p2;

public class App {
    public static void main(String[] args) {
        Participant participant = new Human("Usain Bolt", 150, 200, 10);
//        Participant participant = new Cat("Мурзік", 100, 100, 10);
//        Participant participant = new Dog("Патрон", 160, 195, 18);

        // створюємо масив перешкод
        Obstacle[] obstacles = new Obstacle[20];
        int start = 10;
        for (int i = 0; i < obstacles.length; i++) {
            if (i % 2 == 0) obstacles[i] = new Track(start);
            else obstacles[i] = new Wall(start);
            start = start + 10;
        }

        // запускаємо змагання
        Competition.compete(participant, obstacles);
    }
}
