package hw_24.logging.file;

import hw_24.logging.common.LogLevel;
import hw_24.logging.common.LoggerConfiguration;

public class FileLoggerConfiguration extends LoggerConfiguration {

    public static final int DEFAULT_FILE_SIZE = 1024;
    private final String fileName;
    private final long maxSize;
    public static final String FILE_EXT = ".log";
    public FileLoggerConfiguration(String fileName, long maxSize, LogLevel level, String messageFormat, String timeFormat) {
        super(messageFormat, timeFormat, level);
        this.fileName = fileName;
        this.maxSize = maxSize;
    }

    public String getFileName() {
        return fileName;
    }

    public long getMaxSize() {
        return maxSize;
    }

}
