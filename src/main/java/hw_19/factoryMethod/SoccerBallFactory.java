package hw_19.factoryMethod;

public class SoccerBallFactory extends BallFactory {

    @Override
    public Ball createBall() {
        return new SoccerBall();
    }
}
