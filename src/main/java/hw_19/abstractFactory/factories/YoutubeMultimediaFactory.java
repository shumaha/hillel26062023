package hw_19.abstractFactory.factories;

public class YoutubeMultimediaFactory implements MultimediaFactory {
    @Override
    public Video generateVideo() {
        return new YoutubeVideo();
    }

    @Override
    public Music generateMusic() {
        return new YoutubeMusic();
    }
}
