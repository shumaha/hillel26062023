package hw_14.p2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class App {
    public static void main(String[] args) {
        List<Product> productList = generateProductList();

        System.out.println("Original list: " + productList);
        System.out.println("Filtered list:  " + filterList(productList));
    }

    private static List<Product> generateProductList() {
        List<Product> generatedList = new ArrayList<>();

        generatedList.add(new Product(Product.Type.Book, 256));
        generatedList.add(new Product(Product.Type.Toy, 300));
        generatedList.add(new Product(Product.Type.Toy, 78));
        generatedList.add(new Product(Product.Type.Book, 100));

        Product discountedProduct1 = new Product(Product.Type.Book, 150); discountedProduct1.enableDiscount(10);
        generatedList.add(discountedProduct1);

        Product discountedProduct2 = new Product(Product.Type.Toy, 700); discountedProduct2.enableDiscount(40);
        generatedList.add(discountedProduct2);

        Product discountedProduct3 = new Product(Product.Type.Book, 356); discountedProduct3.enableDiscount(10);
        generatedList.add(discountedProduct3);


        return generatedList;
    }

    private static List<Product> filterList(List<Product> inList) {

        return inList.stream()
                .filter(product -> product.getType() == Product.Type.Book && product.isDiscountEnable() && product.getDiscountValue() == 10)
                .collect(Collectors.toList());
    }


}
