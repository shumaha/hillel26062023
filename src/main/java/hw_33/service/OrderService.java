package hw_33.service;

import hw_33.dto.OrderCreateRequest;
import hw_33.entity.Order;
import hw_33.entity.Product;
import hw_33.repository.OrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.StreamSupport;

import static java.util.Objects.isNull;

@Component
@AllArgsConstructor
public class OrderService {

    private final OrderRepository orderRepository;
    private final ProductService productService;
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"); // 2023-11-19 16:29:35

    public List<Order> showAll() {

        return StreamSupport.stream(orderRepository.findAll().spliterator(), false).toList();
    }

    public Order findOrderById(long id) {
        Optional<Order> optionalOrder = orderRepository.findById(id);

        return optionalOrder.orElse(null);
    }

    public void deleteOrderById(long id) {
        orderRepository.deleteById(id);
    }

    public Order addOrder(OrderCreateRequest orderCreateRequest) {

        Optional<Order> optionalOrder = map(orderCreateRequest);

        return optionalOrder
                .map(orderRepository::save)
                .orElse(null);
    }

    private Optional<Order> map(OrderCreateRequest orderCreateRequest) {

        List<Integer> productsIds = orderCreateRequest.getProducts();

        List<Product> products = new ArrayList<>();
        BigDecimal orderCost = new BigDecimal("0.0");

        for (Integer i: productsIds) {
            Product product = productService.findProductById(i);

            if (isNull(product)) {
                return Optional.empty();
            }

            orderCost = orderCost.add(product.getCost());
            products.add(product);
        }

        return Optional.of(Order.builder()
                .date(LocalDateTime.parse(orderCreateRequest.getDate(), FORMATTER))
                .cost(orderCost)
                .products(products)
                .build());
    }

}
