package hw_18.observer.interfaces;

public interface Observer<E extends Event> {

    void handleEvent(E e);
}
