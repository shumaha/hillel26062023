package hw_15.p1;

import hw_15.animals.*;
import java.util.*;
import java.util.stream.Collectors;

public class App {

    public static void main(String[] args) {
        List<Animal> wildAnimals = (List.of(new Snake(),new Lion(), new Monkey()));
        List<Animal> pets = List.of(new Cat(), new Dog());

        System.out.println("Animals with the highest number of legs: " + findHighestLegs(wildAnimals, pets));
    }

    public static String findHighestLegs(List<Animal> list1, List<Animal> list2) {
        List<Animal> animals = new ArrayList<>(list1);
        animals.addAll(list2);

        Map<Integer, List<Animal>> collected = animals.stream()
                .collect(Collectors.groupingBy(Animal::getLegsCount));

        int maxKey = Collections.max(collected.keySet());

        StringBuilder stringBuilder = new StringBuilder();
        collected.get(maxKey).forEach(t -> stringBuilder.append(t.getClass().getSimpleName()).append(" "));

        return stringBuilder.append("- ").append(maxKey).append(" legs").toString();
    }
}
