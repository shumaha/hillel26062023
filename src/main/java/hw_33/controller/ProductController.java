package hw_33.controller;

import hw_33.entity.Product;
import hw_33.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/products")
@AllArgsConstructor
public class ProductController {

    private final ProductService productService;

    @GetMapping("/show")
    public List<Product> showProducts() {

        return productService.showAll();
    }

    @GetMapping("/show/{id}")
    public Product showProduct(@PathVariable("id") long idProduct) {

        return productService.findProductById(idProduct);
    }
}
