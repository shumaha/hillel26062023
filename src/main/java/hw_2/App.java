package hw_2;

public class App {
    public static void main(String[] args) {

        // 2. Створити клас "Співробітник" з полями: ПІБ, посада, email, телефон, вік.
        // 3. Конструктор класу повинен заповнювати ці поля під час створення об'єкта. Забезпечити інкапсуляцію внутрішніх властивостей класу.
        Employee employee = new Employee("John Smith", "developer", "j.smith@gmail.com", "911", 30);
        System.out.println("Employee: " + employee.getPib() +
                ", position: " + employee.getPosition() +
                ", email: " + employee.getEmail() +
                ", phone: " + employee.getPhone() +
                ", age: " + employee.getAge()
        );

        // 4. Створити два класи з однаковим ім'ям SameName. (Використовувати пакети)
        System.out.println("\nSameName classes:");
        hw_2.sn2.SameName sm1 = new hw_2.sn2.SameName();
        hw_2.sn2.SameName sm2 = new hw_2.sn2.SameName();

        System.out.println("sm1: " + sm1.getClass());
        System.out.println("sm2: " + sm2.getClass());

        // 5. Створити клас Car з публічним методом start
        System.out.println("\nCar");
        new Car().start();
    }
}
