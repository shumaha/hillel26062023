package hw_7;

import java.time.LocalDateTime;

public class ArrayValueCalculator {
    public int doCalc(String[][] matrix) throws ArraySizeException, ArrayDataException {
        checkSizeMatrix(matrix, 4);

        return getMatrixSum(matrix);
    }


    /*
    * Метод перевірки розміру матриці
    */
    private void checkSizeMatrix(Object[][] matrix, final int NEED_LENGTH) throws ArraySizeException {

        // check length of parent array
        if (matrix.length != NEED_LENGTH)
            throw new ArraySizeException("Invalid rows of matrix = " + matrix.length + ". Needs " +NEED_LENGTH+"x"+NEED_LENGTH, "ArraySizeException", LocalDateTime.now());

        // check every element (child array) of parent array (arrays [0][], [1][])
        for (int i = 0; i < matrix.length; i++) {
            Object[] elementArray = matrix[i]; // row of matrix

            // check length of child array
            if (elementArray.length != NEED_LENGTH)
                throw new ArraySizeException("Invalid length of row["+(i+1)+"]=" + elementArray.length+". Needs " +NEED_LENGTH+"x"+NEED_LENGTH, "ArraySizeException", LocalDateTime.now());
        }
    }

    /*
     * Метод отримання суми елементів матриці
     */
    private int getMatrixSum(String[][] matrix) throws ArrayDataException {
        int sum = 0;

        for (int i = 0; i < matrix.length; i++) {
            String[] elementArray = matrix[i]; // row of matrix

            for (int j = 0; j < elementArray.length; j++) {
                try {
                    sum += Integer.parseInt(elementArray[j]);
                } catch (NumberFormatException e) {
                    throw new ArrayDataException("Invalid element ["+i+"]["+j+"]='"+elementArray[j]+"'", e, "ArrayDataException", LocalDateTime.now());
                }
            }
        }

        return sum;
    }
}
