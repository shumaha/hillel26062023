package hw_34.p1;

public class App {
    public static void main(String[] args) {
        Counter counter = new Counter();
        Thread thread1 = new Thread(() -> {
            for (int i = 0; i < 200; i++) {
                counter.increment();
            }
            System.out.println(counter);
        });

        Thread thread2 = new Thread(() -> {
            for (int i = 0; i < 200; i++) {
                counter.decrement();;
            }
            System.out.println(counter);
        });

        thread1.start();
        thread2.start();
    }
}
