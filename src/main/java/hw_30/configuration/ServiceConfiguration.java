package hw_30.configuration;

import hw_30.beans.BankService;
import hw_30.beans.PostService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceConfiguration {

    @Bean
    public PostService postService() {
        return new PostService("Novaposhta service");
    }

    @Bean("Monobank")
    public BankService bankService1() {
        return new BankService("Monobank service");
    }

    @Bean("Privatbank")
    public BankService bankService2() {
        return new BankService("Privatbank service");
    }
}
