package hw_6.p2;

public record Track(int length) implements Obstacle {

    @Override
    public boolean overcome(Participant participant) {
        return participant.getMaxRun() > length;
    }

    @Override
    public int getlength() {
        return length;
    }

    @Override
    public int getDistanceLength() {
        return getlength();
    }

    @Override
    public String toString() {
        return "Track(lenght=" + length +")";
    }
}
