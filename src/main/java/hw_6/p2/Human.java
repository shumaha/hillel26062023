package hw_6.p2;

public class Human extends Participant {

    public Human(String name, int maxRun, int maxJump, int maxJumpCount) {
        super(name, maxRun, maxJump, maxJumpCount);
    }


    @Override
    public String getLabel() {
        return ":>";
    }

    @Override
    public void run() {

    }

    @Override
    public void jump() {
        super.jump();
    }
}
