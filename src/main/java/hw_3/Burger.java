package hw_3;

public class Burger {
    private int bun;
    private int meat;
    private int cheese;
    private int greens;
    private int mayo;


    // private constructor for internal using only
    private Burger(int bun, boolean doubleMeat, int cheese, int greens, int mayo) {
        this.bun = bun;
        this.meat = doubleMeat ? 2:1;
        this.cheese = cheese;
        this.greens = greens;
        this.mayo = mayo;

        printConsistOf(); // it invoked for all constructors
    }

    // dietary burger (without mayo)
    public Burger(boolean doubleMeat, boolean haveMayo) {
        this(1, doubleMeat, 1, 1, haveMayo ? 1:0);
    }

    // double meat burger
    public Burger(boolean doubleMeat) {
        this(doubleMeat, true);
    }

    //burger (all components)
    public Burger() {
        this(false);
    }

    private void printConsistOf() {
        System.out.println("Consist of:"+
                "\nbun: " + bun +
                "\nmeat: " + meat +
                "\ncheese: " + cheese +
                "\ngreens: " + greens +
                "\nmayo: " + mayo
        );
        System.out.println();
    }
}
