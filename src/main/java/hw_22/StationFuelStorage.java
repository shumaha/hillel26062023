package hw_22;

public class StationFuelStorage extends FuelStorage {

    private int minCapacity; // litres

    public StationFuelStorage(int maxCapacity) {
        super(maxCapacity);
        setMinCapacity((int) (maxCapacity*0.1));
    }

    public StationFuelStorage(int minCapacity, int maxCapacity, int currentCapacity) {
        super(maxCapacity, currentCapacity);
        setMinCapacity(minCapacity);
    }

    @Override
    public synchronized void put(int litresToAdd) throws FuelStorageOverPutException {
        super.put(litresToAdd);

        notifyAll();
    }

    public synchronized int take(int litresToNeed) throws FuelStorageEmptyException {
        int takenLitres = litresToNeed;

        int capacityToSet = currentCapacity - litresToNeed;

        if (capacityToSet <= minCapacity) {
            if (capacityToSet <= 0) {
                takenLitres = currentCapacity;
                currentCapacity = 0;
            } else {
                currentCapacity = capacityToSet;
            }

            System.out.println(Thread.currentThread().getName() + " Wait until track will fill up Fuel Storage");
            notifyAll();
        } else {
            currentCapacity = capacityToSet;
        }

        return takenLitres;
    }

    public synchronized void ifNonMinThenWait() {
        if (currentCapacity > minCapacity) {
            try {
                System.out.println(Thread.currentThread().getName() + " turns to wait");
                wait();
            } catch (InterruptedException ignore) {}
        }
    }

    public synchronized void ifMinThenWait() {
        if (currentCapacity <= minCapacity) {
            try {
                System.out.println(Thread.currentThread().getName() + " turns to wait");
                wait();
            } catch (InterruptedException ignore) {}
        }
    }

    public void setMinCapacity(int minCapacity) {
        if (0 > minCapacity || minCapacity > ((int) (getMaxCapacity() * 0.5)))
            throw new IllegalArgumentException("minCapacity can't be < 0 or > 50% of max capacity!");

        this.minCapacity = minCapacity;
    }

    public int getMinCapacity() {
        return minCapacity;
    }
}
