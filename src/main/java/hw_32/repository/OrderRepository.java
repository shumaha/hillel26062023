package hw_32.repository;

import hw_32.entity.Order;
import hw_32.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Component
public class OrderRepository {

    @Autowired
    private DataBaseConnection dataBaseConnection;
    private static DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"); // 2023-11-19 16:29:35
    private static final String FIND_ORDER_BY_ID =
            """
            SELECT o.id, o.order_dt, o.cost, p.id AS product_id, p.name AS product_name, p.cost AS product_cost
            FROM Order_products op
            JOIN Orders o ON o.id = op.order_id
            JOIN Product p ON p.id = op.product_id
            WHERE o.id = ?;
            """;

    private static final String FIND_LAST_ORDER =
            """
            SELECT id, order_dt, cost
            FROM Orders
            WHERE order_dt = ? and cost like ?
            ORDER BY id DESC
            LIMIT 1;
            """;
    private static final String FIND_ALL_ORDERS =
             """
             SELECT o.id, o.order_dt, o.cost, p.id AS product_id, p.name AS product_name, p.cost AS product_cost
             FROM Order_products op
             JOIN Orders o ON o.id = op.order_id
             JOIN Product p ON p.id = op.product_id
             ORDER BY o.id;
             """;
    private static final String INSERT_ORDER = "INSERT into Orders(order_dt, cost) values(?, ?);";
    private static final String INSERT_PRODUCT_BINDINGS = "INSERT INTO Order_products(order_id, product_id) values(?, ?);";

    public Optional<List<Order>> selectAll() {
        try {
            Connection connection = dataBaseConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_ORDERS);

            ResultSet resultSet = preparedStatement.executeQuery();

            Map<Integer, Order> orderMap = new HashMap<>();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");

                if (!orderMap.containsKey(id)) {
                    LocalDateTime order_dt = LocalDateTime.parse(resultSet.getString("order_dt"), FORMATTER);
                    BigDecimal cost = BigDecimal.valueOf(resultSet.getFloat("cost"));

                    orderMap.put(id, new Order(id, order_dt, cost, new ArrayList<>()));

                }

                int product_id = resultSet.getInt("product_id");
                String product_name = resultSet.getString("product_name");
                BigDecimal product_cost = BigDecimal.valueOf(resultSet.getFloat("product_cost"));

                orderMap.get(id).getProducts().add(new Product(product_id, product_name, product_cost));
            }

            if (!orderMap.values().isEmpty()) {
                return Optional.of(new ArrayList<>(orderMap.values()));
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Exception occurred when tries to find orders!");
        }

        return Optional.empty();
    }

    public Optional<Order> findOrderById(int idToFind) {
        try {
            Connection connection = dataBaseConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_ORDER_BY_ID);
            preparedStatement.setInt(1, idToFind);
            ResultSet resultSet = preparedStatement.executeQuery();

            Order order = null;

            while (resultSet.next()) {
                if (isNull(order)) {
                    int id = resultSet.getInt("id");
                    LocalDateTime order_dt = LocalDateTime.parse(resultSet.getString("order_dt"), FORMATTER);
                    BigDecimal cost = BigDecimal.valueOf(resultSet.getFloat("cost"));
                    order = new Order(id, order_dt, cost, new ArrayList<>());
                }

                int product_id = resultSet.getInt("product_id");
                String product_name = resultSet.getString("product_name");
                BigDecimal product_cost = BigDecimal.valueOf(resultSet.getFloat("product_cost"));

                order.getProducts().add(new Product(product_id, product_name, product_cost));
            }

            if (nonNull(order)) {

                return Optional.of(order);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Exception occurred when tries to find order by ID!");
        }

        return Optional.empty();
    }

    public Optional<Order> addOrder(String date, BigDecimal cost, List<Integer> products) {

        cost = cost.setScale(2, RoundingMode.HALF_EVEN);
        int rowAdd;

        try {
            Connection connection = dataBaseConnection.getConnection();

            // INSERT Order
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_ORDER);

            int indexCount = 1;
            preparedStatement.setString(indexCount++, date);
            preparedStatement.setBigDecimal(indexCount, cost);

            rowAdd = preparedStatement.executeUpdate();

            if (rowAdd > 0) {

                // SELECT order id
                indexCount = 1;
                preparedStatement = connection.prepareStatement(FIND_LAST_ORDER);
                preparedStatement.setString(indexCount++, date);
                preparedStatement.setBigDecimal(indexCount, cost);

                ResultSet resultSet = preparedStatement.executeQuery();

                if (resultSet.next()) {
                    int id = Integer.parseInt(resultSet.getString("id"));

                    // INSERT product ids
                    preparedStatement = connection.prepareStatement(INSERT_PRODUCT_BINDINGS);

                    for (int i: products) {
                        indexCount = 1;
                        preparedStatement.setInt(indexCount++, id);
                        preparedStatement.setInt(indexCount, i);
                        preparedStatement.addBatch();
                    }

                    int[] ints = preparedStatement.executeBatch();

                    if (ints.length == products.size()) {
                        return findOrderById(id);
                    }
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Exception occurred when tries to create order!");
        }

        return Optional.empty();
    }

}

