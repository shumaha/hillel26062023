package hw_19.templateMethod.games.TikTakToe;

public class ConflictInputException extends Exception {
    public ConflictInputException(String message) {
        super(message);
    }
}
