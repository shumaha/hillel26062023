package hw_6.p2;

/**
 * Перешкода
 */
public interface Obstacle {

    boolean overcome(Participant participant);
    int getlength();
    int getDistanceLength();
}
