package hw_21;

public class App {
    public static void main(String[] args) throws InterruptedException {
        String fileToRead = "data.txt";

        Thread threadCountWords = new Thread(new FileCountWordsHandler(fileToRead, FileCountWordsHandler.class.getSimpleName() + "_result.txt"));
        Thread threadCountChars = new Thread(new FileCountCharsHandler(fileToRead, FileCountCharsHandler.class.getSimpleName() + "_result.txt"));
        Thread threadCountLines = new Thread(new FileCountLinesHandler(fileToRead, FileCountLinesHandler.class.getSimpleName() + "_result.txt"));

        threadCountLines.start();
        threadCountWords.start();
        threadCountChars.start();
    }
}
