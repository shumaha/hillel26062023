package hw_24.logging.stdout;

import hw_24.logging.common.LogLevel;
import hw_24.logging.common.LoggerConfiguration;

public class StdoutLoggerConfiguration extends LoggerConfiguration {

    private final String color;
    public StdoutLoggerConfiguration(String color, String currentMessageFormat, String currentDateFormat, LogLevel level) {
        super(currentMessageFormat, currentDateFormat, level);
        this.color = color;
    }

    public String getColor() {
        return color;
    }
}
