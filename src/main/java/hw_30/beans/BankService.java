package hw_30.beans;

public class BankService {
    private final String serviceName;


    public BankService(String serviceName) {
        this.serviceName = serviceName;
    }

    @Override
    public String toString() {
        return "BankService{" +
                "serviceName='" + serviceName + '\'' +
                '}';
    }
}
