package hw_19.factoryMethod;

public abstract class BallFactory {
    abstract Ball createBall();
}
