package hw_7;

import java.time.LocalDateTime;

public class ArrayDataException extends Exception {
    private final String name;
    private final LocalDateTime localDateTime;

    public ArrayDataException(String name, LocalDateTime localDateTime) {
        this.name = name;
        this.localDateTime = localDateTime;
    }

    public ArrayDataException(String message, String name, LocalDateTime localDateTime) {
        super(message);
        this.name = name;
        this.localDateTime = localDateTime;
    }

    public ArrayDataException(String message, Throwable cause, String name, LocalDateTime localDateTime) {
        super(message, cause);
        this.name = name;
        this.localDateTime = localDateTime;
    }

    public ArrayDataException(Throwable cause, String name, LocalDateTime localDateTime) {
        super(cause);
        this.name = name;
        this.localDateTime = localDateTime;
    }

    public ArrayDataException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String name, LocalDateTime localDateTime) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.name = name;
        this.localDateTime = localDateTime;
    }

    @Override
    public String toString() {
        return "ArrayDataException{" +
                "name='" + name + '\'' +
                ", localDateTime='" + localDateTime + '\'' +
                ", message='" + this.getMessage() + '\'' +
                '}';
    }
}
