package hw_19.templateMethod;

import hw_19.templateMethod.games.Game;
import hw_19.templateMethod.games.TikTakToe.TikTakToe;

public class App {
    public static void main(String[] args) {
        Game game = new TikTakToe();
        game.startGame();
    }
}
