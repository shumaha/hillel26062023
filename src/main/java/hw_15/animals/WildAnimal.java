package hw_15.animals;

public abstract class WildAnimal extends Animal {

    public WildAnimal(int legsCount) {
        super(legsCount);
    }
}
