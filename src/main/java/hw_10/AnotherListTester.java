package hw_10;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class AnotherListTester {

    public static void test() {
//        testAdd1();
//        testAdd2();
//        testAddAll_1();
//        testAddAll_2();
//        testGet();
//        testSet();
//        testIterator();
//        testSizeAndIsEmpty();
//        testContains();
//        testContainsAll();
//        testToArray1();
        testToArray2();
//        testRemove_1();
//        testRemove_2();
//        testRemoveAll();
//        testRetainAll();
//        testClear();
//        testIndexOf();
    }

    private static void testAdd1() {
        List<String> myList = new AnotherList<>();
        System.out.println("before" + myList);
        for (int i = 0; i < 20; i++) {
            myList.add(Integer.toString(i));
        }

        System.out.println("after" + myList); // toString()
    }

    private static void testAdd2() {
        AnotherList<String> myList = generateAnotherList(20);
        System.out.println("before" + myList);
        myList.add(6, "55");
        System.out.println("after" + myList);
    }

    private static void testAddAll_1() {
        AnotherList<String> myList = generateAnotherList(20);
        System.out.println("before " + myList);

        AnotherList<String> myListToAdd = new AnotherList<>();
        for (int i = 20; i <= 50; i++) {
            myListToAdd.add(Integer.toString(i));
        }
        boolean result = myList.addAll(myListToAdd);
        System.out.println("after " + myList);
        System.out.println("Result: " + result);
    }

    private static void testAddAll_2() {
        AnotherList<String> myList = generateAnotherList(20);
        System.out.println("before " + myList);

        AnotherList<String> myListToAdd = new AnotherList<>();
        for (int i = 90; i < 92; i++) {
            myListToAdd.add(Integer.toString(i));
        }
        boolean result = myList.addAll(0, myListToAdd);
        System.out.println("after " + myList);
        System.out.println("Result: " + result);
    }

    private static void testRemove_1() {
        AnotherList<String> myList = generateAnotherList(20);
        System.out.println("before " + myList);

        int indexToRemove = 18;
        String removedElement = myList.remove(indexToRemove);
        System.out.println("after " + myList);
        System.out.println("Removed element["+indexToRemove+"]: " + removedElement);
    }

    private static void testRemove_2() {
        AnotherList<String> myList = generateAnotherList(20);
        System.out.println("before " + myList);

        String elementToRemove = "5";
        boolean result = myList.remove(elementToRemove);
        System.out.println("after " + myList);
        System.out.println("Removed element["+elementToRemove+"]: " + result);
    }

    private static void testRemoveAll() {
        AnotherList<String> myList = generateAnotherList(5);
        myList.add("1");
        myList.add("2");
        System.out.println("before " + myList);

        AnotherList<String> elementsToRemove = new AnotherList<>();
        elementsToRemove.add("1");
        elementsToRemove.add("2");
        boolean result = myList.removeAll(elementsToRemove);
        System.out.println("after " + myList);
        System.out.println("Removed element["+elementsToRemove+"]: " + result);
    }

    private static void testRetainAll() {
        AnotherList<String> myList = generateAnotherList(5);
        myList.add("1");
        myList.add("2");
        System.out.println("before " + myList);

        AnotherList<String> elementsToRetain = new AnotherList<>();
        elementsToRetain.add("0");
        elementsToRetain.add("2");
        elementsToRetain.add("3");
        boolean result = myList.retainAll(elementsToRetain);
        System.out.println("after " + myList);
        System.out.println("elementsToRetain: " + elementsToRetain + ", result: " + result);
    }

    private static void testClear() {
        AnotherList<String> myList = generateAnotherList(10);
        System.out.println("before " + myList);
        myList.clear();
        System.out.println("after " + myList);
    }

    private static void testGet() {
        AnotherList<String> myList = generateAnotherList(10);
        System.out.println(myList);
        int index = 5;
        System.out.println("Element["+index+"], value=" + myList.get(index));
    }

    private static void testSet() {
        AnotherList<String> myList = generateAnotherList(10);
        System.out.println("before " + myList);
        int index = 5;
        String prev = myList.set(index, "55");
        System.out.println("after " + myList);
        System.out.println("Previous element["+index+"], value=" + prev);
    }

    private static void testIndexOf() {
        AnotherList<String> myList = generateAnotherList(10);
        String s = "999";
        int index = 7;
        myList.add(index, s);
        System.out.println(myList);
        System.out.println("Element["+s+"], setted index["+index+"], founded index=" + myList.indexOf(s));
    }

    private static void testIterator() {
        AnotherList<String> myList = generateAnotherList(20);
        Iterator<String> iterator = myList.iterator();
        while (iterator.hasNext())
            System.out.print(iterator.next() + " ");
    }

    private static void testSizeAndIsEmpty() {
        AnotherList<String> myList = generateAnotherList(20);
        System.out.println(myList + ", size(): " + myList.size() + ", isEmpty: " + myList.isEmpty());
        AnotherList<String> myEmptyList = new AnotherList<>();
        System.out.println(myEmptyList + ", size(): " + myEmptyList.size() + ", isEmpty: " + myEmptyList.isEmpty());
    }

    private static void testContains() {
        AnotherList<String> myList = generateAnotherList(20);
        String forContains = "6";
        System.out.println(myList + ", contains("+forContains+"): " + myList.contains(forContains));
        forContains = "99";
        System.out.println(myList + ", contains("+forContains+"): " + myList.contains(forContains));
    }

    private static void testContainsAll() {
        AnotherList<String> myList = generateAnotherList(20);
        System.out.println(myList);

        AnotherList<String> elementsToContains = new AnotherList<>();
        elementsToContains.add("1");
        elementsToContains.add("2");
        boolean result = myList.containsAll(elementsToContains);
        System.out.println("Contains elements["+elementsToContains+"]: " + result);
    }

    private static void testToArray1() {
        AnotherList<String> myList = generateAnotherList(20);
        Object[] newObj = myList.toArray();
        System.out.println(myList);
        System.out.println("Object[]: " + Arrays.toString(newObj));
    }

    private static void testToArray2() {
        AnotherList<String> myList = generateAnotherList(20);

        // array 1 test
        String[] array1 = new String[10];
        System.out.println(myList);
        System.out.println("array1 before [size("+array1.length+")]: " + Arrays.toString(array1));
        array1 = myList.toArray(array1);
        System.out.println("array1 after [size("+array1.length+")]: " + Arrays.toString(array1));

        // array 2 test
        String[] array2 = new String[myList.size()+2];
        array2[array2.length-2]="pre_last_element";
        array2[array2.length-1]="last_element";
        System.out.println("array2 before [size("+array2.length+")]: " + Arrays.toString(array2));
        array2 = myList.toArray(array2);
        System.out.println("array2 after [size("+array2.length+")]: " + Arrays.toString(array2));
    }

    private static AnotherList<String> generateAnotherList(int cnt) {
        AnotherList<String> myList = new AnotherList<>(cnt);

        for (int i = 0; i < cnt; i++) {
            myList.add(Integer.toString(i));
        }

        return myList;
    }
}
