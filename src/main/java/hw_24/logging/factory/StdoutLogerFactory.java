package hw_24.logging.factory;

import hw_24.logging.common.Logger;
import hw_24.logging.file.FileLogger;
import hw_24.logging.stdout.StdoutLogger;
import hw_24.logging.stdout.StdoutLoggerConfiguration;

import java.io.IOException;

public class StdoutLogerFactory {

    public static Logger getLogger(Class<?> clazz) {
        try {
            return new StdoutLogger(clazz);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
