package hw_17.p7;

import hw_17.p7.logging.*;

import java.io.IOException;

public class App {
    public static void main(String[] args) throws IOException {
        FileLoggerConfiguration fileLoggerConfiguration;
//        fileLoggerConfiguration = new FileLoggerConfiguration("SingleLogFile.txt", LogLevel.INFO, 1024, "[CURRENT_TIME]\t[LEVEL]\tПовідомлення: [MESSAGE_STRING]");
        fileLoggerConfiguration = new FileLoggerConfigurationLoader("FileLogger.conf").load();

        Logger logger = new FileLogger(fileLoggerConfiguration);

        // Usage example of StdoutXXX
        //Logger outLogger = new StdoutLogger(new StdoutLoggerConfiguration());
        //Logger outLoggerFromFileConf = new StdoutLogger(new StdoutLoggerConfigurationLoader("StdOutLogger.conf"));


        // log messages
        logger.info("Warning message");
        logger.debug("Error message");
        logger.debug("Error message 2");

        logger.close();
    }
}
