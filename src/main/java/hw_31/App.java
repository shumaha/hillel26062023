package hw_31;


import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
    public static final String PACKAGE_TO_SCAN = "hw_31";
    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(PACKAGE_TO_SCAN);
        applicationContext.getBean(CartController.class).start();
    }
}
