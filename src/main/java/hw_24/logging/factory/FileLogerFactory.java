package hw_24.logging.factory;

import hw_24.logging.file.FileLogger;
import hw_24.logging.common.Logger;

import java.io.IOException;

public class FileLogerFactory {

    public static Logger getLogger(Class<?> clazz) {
        try {
            return new FileLogger(clazz);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
