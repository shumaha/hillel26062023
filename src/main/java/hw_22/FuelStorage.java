package hw_22;

public abstract class FuelStorage {
    private final int maxCapacity; // litres
    protected int currentCapacity; // litres


    public FuelStorage(int maxCapacity) {
        this.maxCapacity = maxCapacity;
        this.currentCapacity = 0;
    }

    public FuelStorage(int maxCapacity, int currentCapacity) {
        this(maxCapacity);
        this.currentCapacity = currentCapacity;
    }

    public void put(int litresToAdd) throws FuelStorageOverPutException {

        if ( (currentCapacity + litresToAdd) > maxCapacity) {
            throw new FuelStorageOverPutException("Capacity to add is greater then max capacity ("+litresToAdd+" > "+maxCapacity+")!");
        }

        currentCapacity += litresToAdd;
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }

    public int getCurrentCapacity() {
        return currentCapacity;
    }

    public int getLitresToMax() {
        return maxCapacity - currentCapacity;
    }
}
