package hw_12;

import java.util.*;

public class MyLinkedList<E> implements List<E> {

    private int size = 0;

    /**
     * First node.
     */
    private Node<E> first;

    /**
     * Last node.
     */
    private Node<E> last;



    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        if (o == null) {
            Node<E> current = first;
            while (current != null) {
                if (current.item == null)
                    return true;

                current = current.next;
            }
        } else {
            Node<E> current = first;
            while (current != null) {
                if (o.equals(current.item))
                    return true;

                current = current.next;
            }
        }

        return false;
    }

    @Override
    public Iterator<E> iterator() {
        return new IteratorImpl();
    }

    private class IteratorImpl implements Iterator<E> {
        Node<E> nextIteratorNode = first;

        @Override
        public boolean hasNext() {
            return nextIteratorNode != null;
        }

        @SuppressWarnings("unchecked")
        @Override
        public E next() {
            E item = nextIteratorNode.item;
            nextIteratorNode = nextIteratorNode.next;

            return item;
        }
    }

    @Override
    public Object[] toArray() {
        Object[] arrayToReturn = new Object[size];

        Node<E> current = first;
        int i = 0;
        while (current != null) {
            arrayToReturn[i] = current.item;
            current = current.next;
            i++;
        }

        return arrayToReturn;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T[] toArray(T[] a) {

        Object[] arrayToReturn = new Object[size];

        Node<E> current = first;
        int i = 0;
        while (current != null) {
            arrayToReturn[i] = current.item;
            current = current.next;
            i++;
        }

        if (a.length < size) {
            return (T[]) Arrays.copyOf(arrayToReturn, size, a.getClass());
        }

        System.arraycopy(arrayToReturn, 0, a, 0, size);
        if (a.length > size)
            a[size] = null;

        return a;
    }

    @Override
    public boolean add(E e) {
        addLast(e);

        return true;
    }

    private void addLast(E e) {
        Node<E> lastOld = last;
        Node<E> newNode = new Node<>(lastOld, e, null);
        last = newNode;
        if (size == 0)
            first = newNode;
        else
            lastOld.next = newNode;

        size++;
    }

    @Override
    public boolean remove(Object o) {
        Node<E> current = first;

        if (o == null) {
            while (current != null) {
                if (current.item == null) {
                    unlinkNode(current);
                    return true;
                }
                current = current.next;
            }
        } else {
            while (current != null) {
                if (o.equals(current.item)) {
                    unlinkNode(current);
                    return true;
                }
                current = current.next;
            }
        }

        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        if (c == null)
            throw new NullPointerException("Collection is NULL!");

        Object[] objects = c.toArray();
        for (Object o: objects) {
            if (!contains(o))
                return false;
        }

        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        if (c == null)
            throw new NullPointerException("Collection is NULL!");

        for (E e : c) {
            addLast(e);
        }

        size += c.size();

        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        Objects.checkIndex(index, size);

        if (c == null)
            throw new NullPointerException("Collection is NULL!");


        if (index == size) {
            for (E e : c)
                addLast(e);
        }
        else {
            int i = index;
            Node<E> nodeByIndex = getNodeByIndex(index);
            Node<E> prevNode = nodeByIndex.prev;

            for (E e : c) {
                Node<E> newNode = new Node<>(prevNode, e, nodeByIndex);

                if (i == 0)
                    first = newNode;
                else
                    prevNode.next = newNode;

                prevNode = newNode;
                i++;
            }
        }

        size += c.size();

        return true;

        /*
        Objects.checkIndex(index, size);

        if (index == size)
            addLast(element);
        else {
            Node<E> nodeByIndex = getNodeByIndex(index);
            Node<E> prevNode = nodeByIndex.prev;

            Node<E> newNode = new Node<>(prevNode, element, nodeByIndex);

            if (index == 0)
                first = newNode;
            else
                prevNode.next = newNode;

            nodeByIndex.prev = newNode;
        }
        size++;
        */
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean isRemoved = false;
        Node<E> current = first;

        while (current != null) {
            Node<E> next = current.next;

            if (c.contains(current.item)) {
                unlinkNode(current);
                isRemoved = true;
            }
            current = next;
        }

        return isRemoved;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean isRemoved = false;
        Node<E> current = first;

        while (current != null) {
            Node<E> next = current.next;

            if (!c.contains(current.item)) {
                unlinkNode(current);
                isRemoved = true;
            }
            current = next;
        }

        return isRemoved;
    }

    @Override
    public void clear() {
        Node<E> current = first;

        while (current != null) {
            Node<E> next = current.next;

            current.next = null;
            current.prev = null;
            current.item = null;

            current = next;
        }

        first = last = null;
        size = 0;

    }

    @Override
    public E get(int index) {
        Objects.checkIndex(index, size);

        return getNodeByIndex(index).item;
    }

    @Override
    public E set(int index, E newValue) {
        Objects.checkIndex(index, size);

        Node<E> node = getNodeByIndex(index);
        E oldValue = node.item;
        node.item = newValue;

        return oldValue;
    }

    @Override
    public void add(int index, E element) {
        Objects.checkIndex(index, size);

        if (index == size)
            addLast(element);
        else {
            Node<E> nodeByIndex = getNodeByIndex(index);
            Node<E> prevNode = nodeByIndex.prev;

            Node<E> newNode = new Node<>(prevNode, element, nodeByIndex);

            if (index == 0)
                first = newNode;
            else
                prevNode.next = newNode;

            nodeByIndex.prev = newNode;
        }
        size++;
    }

    @Override
    public E remove(int index) {
        Objects.checkIndex(index, size);

        Node<E> nodeByIndex = getNodeByIndex(index);

        return unlinkNode(nodeByIndex);
    }

    private E unlinkNode (Node<E> node) {

        Node<E> prevNode = node.prev;
        Node<E> nextNode = node.next;

        if (prevNode == null) {
            first = nextNode;
            first.prev = null;
        } else {
            prevNode.next = nextNode;
        }

        if (nextNode == null) {
            last = prevNode;
            last.next = null;
        } else {
            nextNode.prev = prevNode;
        }

        size--;

        E item = node.item;
        node.next = null;
        node.prev = null;
        node.item = null;

        return item;
    }

    @Override
    public int indexOf(Object o) {
        Node<E> current = first;

        int i = 0;
        if (o == null) {
            while (current != null) {
                if (current.item == null) {
                    return i;
                }
                current = current.next;
                i++;
            }
        } else {
            while (current != null) {
                if (o.equals(current.item)) {
                    unlinkNode(current);
                    return i;
                }
                current = current.next;
                i++;
            }
        }

        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        Node<E> current = last;

        int i = size-1;
        if (o == null) {
            while (current != null) {
                if (current.item == null) {
                    return i;
                }
                current = current.prev;
                i--;
            }
        } else {
            while (current != null) {
                if (o.equals(current.item)) {
                    unlinkNode(current);
                    return i;
                }
                current = current.prev;
                i--;
            }
        }

        return -1;
    }

    @Override
    public ListIterator<E> listIterator() {
        return null;
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return null;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return null;
    }

    private Node<E> getNodeByIndex(int index) {
        Node<E> current = first;
        for (int i = 0; i < index; i++) {
            current = current.next;
        }

        return current;
    }

    private Node<E> getNodeByValue(E value) {
        Node<E> current = first;

        if (value == null) {
            while (current != null) {
                if (current.item == null) {
                    return current;
                }
                current = current.next;
            }
        } else {
            while (current != null) {
                if (value.equals(current.item)) {
                    return current;
                }
                current = current.next;
            }
        }

        return null;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder(this.getClass().getSimpleName() +"["+size()+"]: {");

        Node<E> current = first;
        while (current != null) {
            builder.append(current.item).append(',');
            current = current.next;
        }

        if (size > 0)
            builder.deleteCharAt(builder.length()-1);

        return builder.append("}").toString();
    }

    private static class Node<E> {
        E item;
        Node<E> next;
        Node<E> prev;

        Node(Node<E> prev, E element, Node<E> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }
    }


}
