package hw_6.p2;

public record Wall(int length) implements Obstacle {

    @Override
    public boolean overcome(Participant participant) {
        participant.jump();

        return participant.getMaxJump() > length && participant.getMaxJumpCount()>-1;
    }

    @Override
    public int getlength() {
        return length;
    }

    @Override
    public int getDistanceLength() {
        return 0;
    }

    public String toString() {
        return "Wall(lenght=" + length +")";
    }
}
