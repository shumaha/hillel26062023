package hw_18.decorator;

import hw_18.decorator.interfaces.SQLRequestFormatter;

public class TrimSQLFormatter implements SQLRequestFormatter {

    @Override
    public String format(String sqlRequest) {
        return sqlRequest.trim();
    }
}
