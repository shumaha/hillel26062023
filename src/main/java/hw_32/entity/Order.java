package hw_32.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class Order {
    private final int id;
    private final LocalDateTime date;
    private final BigDecimal cost;
    private final List<Product> products;
}
