package hw_9.p2;

public abstract class Fruit {
    private final float weigth;

    public Fruit(float weigth) {
        this.weigth = weigth;
    }

    public float getWeigth() {
        return weigth;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " weight: " + weigth;
    }
}
