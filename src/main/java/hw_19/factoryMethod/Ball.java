package hw_19.factoryMethod;

public abstract class Ball {
    private final String name;

    public Ball(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Ball{" +
                "name='" + name + '\'' +
                '}';
    }
}
