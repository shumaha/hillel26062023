package hw_14.p3;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class App {
    public static void main(String[] args) {
        List<Product> productList = generateProductList();

        System.out.println("Original list: " + productList);

        try {
            System.out.println("Found product:  " + findCheepest(productList, Product.Type.Book));
        } catch (NoProductFindException e) {
            System.out.println("Catch exception:  " + e.getMessage());
        }
    }

    private static List<Product> generateProductList() {
        List<Product> generatedList = new ArrayList<>();

        generatedList.add(new Product(Product.Type.Toy, 256));
        generatedList.add(new Product(Product.Type.Toy, 300));
        generatedList.add(new Product(Product.Type.Toy, 78));
        generatedList.add(new Product(Product.Type.Book, 100));

        Product discountedProduct1 = new Product(Product.Type.Toy, 150); discountedProduct1.enableDiscount(10);
        generatedList.add(discountedProduct1);

        Product discountedProduct2 = new Product(Product.Type.Toy, 700); discountedProduct2.enableDiscount(40);
        generatedList.add(discountedProduct2);

        Product discountedProduct3 = new Product(Product.Type.Book, 150); discountedProduct3.enableDiscount(50);
        generatedList.add(discountedProduct3);


        return generatedList;
    }

    private static Product findCheepest(List<Product> inList, Product.Type type) throws NoProductFindException {

        Optional<Product> optionalProduct = inList.stream()
                .filter(product -> product.getType() == type)
                .min((product1, product2) -> Double.compare(product1.getFinalPrice(), product2.getFinalPrice()));

        if (optionalProduct.isEmpty())
            throw new NoProductFindException("Product [category: "+type+"] is not found!");

        return optionalProduct.get();
    }


}
