package hw_19.abstractFactory.factories;

public class LocalMusic extends Music {
    protected LocalMusic() {
        super("Local");
    }
}
