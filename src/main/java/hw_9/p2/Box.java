package hw_9.p2;

import static java.util.Objects.isNull;

public class Box <T extends Fruit> {
    private final Object[] boxArray;

    /**
     * Pointer to the next index of {@link #boxArray}
     * <br>After init pointer = 0
     */
    private int pointer;

    /**
     * Size of the box (size of array {@link #boxArray})
     * <br>Can be values of enum {@link BoxSize}
     */
    private final BoxSize boxSize;


    public Box(BoxSize boxSize) {
        this.boxArray = new Object[boxSize.size];
        this.boxSize = boxSize;
    }

    private int getFreeSpace() {

        return boxArray.length - pointer;
    }

    private int getTakenSpace() {

        return pointer;
    }

    public boolean add(T fruit) {
        if (isNull(fruit)) {
            System.out.println("Argument can't be NULL!");

            return false;
        }

        if (pointer < boxArray.length) {
            boxArray[pointer++] = fruit;

            return true;
        }

        System.out.println("Box haven't enough space!");

        return false;
    }

    public boolean add(T... fruits) {
        // check array is not null
        if (isNull(fruits)) {
            System.out.println("Argument can't be NULL!");

            return false;
        }

        // check every element is not null
        for (T el: fruits) {
            if (isNull(el)) {
                System.out.println("Element of array can't be NULL!");

                return false;
            }
        }

        if (getFreeSpace() >= fruits.length) {
            for (T fruit: fruits)
                add(fruit);

            return true;
        }

        System.out.println(this + " haven't enough space!!");
        return false;
    }

    @SuppressWarnings("unchecked")
    public float getWeight() {
        float boxWeight = 0;
        for (int i = 0; i < pointer; i++) {
            boxWeight += ((T) boxArray[i]).getWeigth();
        }

        return boxWeight;
    }

    public boolean compare(Box<?> boxToCompare) {
        return getWeight() == boxToCompare.getWeight();
    }

    public boolean merge(Box<T> boxToMerge) {
        if (this.equals(boxToMerge)) {
            System.out.println("Boxes can't be the same!");

            return false;
        }


        if (boxToMerge.getTakenSpace() > getFreeSpace()) {
            System.out.println(this+" to merge haven't enough space!");

            return false;
        }


        while (true) {
            T el = boxToMerge.takeOutFruit();
            if (!isNull(el))
                add(el);
            else
                break;
        }

        return true;
    }

    @SuppressWarnings("unchecked")
    private T takeOutFruit() {

        return pointer > 0 ? (T) boxArray[--pointer] : null;
    }

    @Override
    public String toString() {
        return "Box{" +
                "size: " + boxSize + "("+boxSize.size+"), " +
                "cnt of friuts=" + getTakenSpace() +
                '}';
    }

    public enum BoxSize {
        BIG(100),
        NORMAL(70),
        LITTLE(40);

        private final int size;

        BoxSize(int size) {
            this.size = size;
        }
    }
}
