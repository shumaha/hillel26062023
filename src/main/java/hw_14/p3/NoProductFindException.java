package hw_14.p3;

public class NoProductFindException extends Exception {


    public NoProductFindException(String message) {
        super(message);
    }

    public NoProductFindException(String message, Throwable cause) {
        super(message, cause);
    }
}
