package hw_19.abstractFactory.factories;

public class LocalMultimediaFactory implements MultimediaFactory {

    @Override
    public Video generateVideo() {
        return new LocalVideo();
    }

    @Override
    public Music generateMusic() {
        return new LocalMusic();
    }
}
