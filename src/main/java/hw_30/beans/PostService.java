package hw_30.beans;

public class PostService {

    private final String serviceName;

    public PostService(String serviceName) {
        this.serviceName = serviceName;
    }

    @Override
    public String toString() {
        return "PostService{" +
                "serviceName='" + serviceName + '\'' +
                '}';
    }
}
