package hw_24.logging.file;

import hw_24.logging.common.LogLevel;
import hw_24.logging.common.Logger;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static java.util.Objects.isNull;

public class FileLogger extends Logger {
    private final FileLoggerConfiguration conf;
    private OutputStream outputStream;
    private final DateTimeFormatter dateFormatter;
    private final DateTimeFormatter newFileNameformatter = DateTimeFormatter.ofPattern("dd.MM.yyyy-HH.mm");
    private long writedBytesCount;
    private final String formatPattern;
    private final Class<?> clazz;

    public FileLogger(Class<?> clazz) throws IOException {
        this.conf = new FileLoggerConfigurationLoader().load("logging.properties");
        this.clazz = clazz;
        this.dateFormatter = DateTimeFormatter.ofPattern(conf.getCurrentDateFormat());
        this.formatPattern = generateFormatPattern(conf.getCurrentMessageFormat());

        File file = new File( conf.getFileName());
        writedBytesCount = file.length();

        try {
            outputStream = new BufferedOutputStream(
                    new FileOutputStream(file, true), 1000);
        }
        catch (IOException e) {
            throw new RuntimeException("Can't create stream for writing log to file ("+file+")!");
        }
    }

    public void info(String message) {
        if (canLog(LogLevel.INFO)) {
            writeMessage(templateMessage(message, LogLevel.INFO));
        }
    }

    public void debug(String message) {
        if (canLog(LogLevel.DEBUG)) {
            writeMessage(templateMessage(message, LogLevel.DEBUG));
        }
    }

    private void writeMessage(String message) {
        byte[] bytes = message.getBytes(StandardCharsets.UTF_8);

        if ( (writedBytesCount+bytes.length) >= conf.getMaxSize()) {
            outputStream = getNewOutputStream(bytes.length);
        }

        writedBytesCount += bytes.length;

        try {
            outputStream.write(bytes);
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean canLog(LogLevel level) {

        return level.value >= conf.getLevel().value;
    }

    private String templateMessage(String message, LogLevel level) {
        String templetedStr = MessageFormat.format
                (formatPattern,
                        LocalDateTime.now().format(dateFormatter),
                        clazz.getName(),
                        message,
                        level.name()
                );

        return templetedStr + System.lineSeparator();
    }

    public String generateFormatPattern(String propertyFormat) {
        String[] propList = new String[]{"time", "className", "message","level"};

        String templetedStr = propertyFormat;
        for (int i = 0; i < propList.length; i++) {
            templetedStr = templetedStr.replaceAll("(?i)" + "\\{"+propList[i]+"}", "{"+i+"}");
        }

        return templetedStr;
    }

    private File getNewFile(String fullFileName, int countToWrite) {

        int lastIndex = fullFileName.lastIndexOf('\\');
        String dirName = fullFileName.substring(0, lastIndex+1);
        if (dirName.isEmpty())
            dirName = ".\\";

        String fileName = fullFileName.substring(lastIndex+1);
        lastIndex = fileName.lastIndexOf('.');
        String firstPartName = fileName.substring(0, lastIndex);
        String fileExtension = fileName.substring(lastIndex);

        String newFileName = firstPartName + "_" + LocalDateTime.now().format(newFileNameformatter);

        String postFix = ""; int id = 1;
        while (true) {
            String fileNameToTest = newFileName + postFix + fileExtension;
            File newFile = checkNewFileName(dirName, fileNameToTest);

            if (isNull(newFile)) {
                return new File(dirName, fileNameToTest);
            }
            // validate max size of file
            else if (newFile.length()+countToWrite < conf.getMaxSize()) {
                return newFile;
            }

            postFix = "_"+id++;
        }
    }

    private OutputStream getNewOutputStream(int countToWrite) {

        File file = getNewFile(conf.getFileName(), countToWrite);
        writedBytesCount = file.length();

        try {
            return
                    new BufferedOutputStream(
                            new FileOutputStream(file, true), 50);
        }
        catch (IOException e) {
            throw new RuntimeException("Can't create stream for writing log to file ("+file+")!");
        }
    }

    public File checkNewFileName(String dirPath, String fileNameToCheck) {
        File dir = new File(dirPath);
        FilenameFilter filter = (d, name) -> name.equals(fileNameToCheck);
        String[] list = dir.list(filter);

        if (list.length > 0) {
            return new File(dirPath, fileNameToCheck);
        }

        return null;
    }
}
