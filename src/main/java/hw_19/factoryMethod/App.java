package hw_19.factoryMethod;

public class App {

    public static void main(String[] args) {
        BallFactory ballFactory = new SoccerBallFactory();
        Ball ball = ballFactory.createBall();
        System.out.println(ball);
    }
}
