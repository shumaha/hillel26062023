package hw_19.templateMethod.games.TikTakToe;

import hw_19.templateMethod.games.TwoPersonGame;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class TikTakToe extends TwoPersonGame {
    private String[][] matrix;
    private Scanner scanner;
    private int maxSteps;
    private final int MATRIX_LENGTH = 3;
    private String[] players = new String[]{"o","x"};
    private String winner = null;
    private int[][] winnerLine;

    @Override
    protected void initGame() {
        matrix = new String[MATRIX_LENGTH][MATRIX_LENGTH];
        maxSteps = MATRIX_LENGTH * MATRIX_LENGTH;
        scanner = new Scanner(System.in);
        players = shufflePlayers(players);
        winnerLine = new int[MATRIX_LENGTH][2];
    }

    @Override
    protected void playGame() {
        int countStep = 1;

        String exception = "";
        while (countStep <= maxSteps) {
            String playerChar = players[(countStep-1)%2];

            clrscr();
            printMatrix(exception + "Player["+playerChar+"] goes ...");

            try {
                int x = getNumber("Set x: ", 1, MATRIX_LENGTH);
                int y = getNumber("Set y: ", 1, MATRIX_LENGTH);

                checkConflict(x-1, y-1);
                matrix[y-1][x-1] = playerChar;

                if (checkWin(playerChar,x-1, y-1) ) {
                    winner = playerChar;
                    break;
                }


            } catch (OutbundInputException | ConflictInputException e) {
                exception = "Error!!! " + e.getMessage() + "\n";
                continue;
            } catch (InputMismatchException e) {
                exception = "Error!!! Input value must be INTEGER NUMBER!\n";
                scanner.next();
                continue;
            }

            countStep++;
            exception = "";
        }
    }

    @Override
    protected void endGame() {
        // strings for red color
        final String ANSI_RED = "\u001B[31m";
        final String ANSI_RESET = "\u001B[0m";

        if (nonNull(winner)) {
            for (int[] ints : winnerLine) {
                int x = ints[0];
                int y = ints[1];
                matrix[y][x] = ANSI_RED + matrix[y][x].toUpperCase() + ANSI_RESET;
            }
        }

        clrscr();
        printMatrix("End game");
    }

    @Override
    protected void printWinner() {
        if (isNull(winner)) {
            System.out.println("The game ended in a draw.");
        } else {
            System.out.println("Player["+winner+"] wins!!!");
        }
    }

    private String filterNull(String character) {
        if (isNull(character)) {
            return " ";
        }

        return character;
    }

    private void printMatrix(String bottomMessage) {
        StringBuilder topBuilder = new StringBuilder("   ");
        for (int i = 1; i <= matrix.length; i++) {
            topBuilder.append(" ").append(i).append(" ");
        }
        String top = topBuilder.append(" x").toString();
        System.out.println(top);

        // print matrix
        for (int i = 0; i < matrix.length; i++) {
            System.out.print((i+1) + "  ");
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print("[" + filterNull(matrix[i][j]) + "]");
            }
            System.out.println();
        }
        System.out.println("y");
        System.out.println("_______________________");
        System.out.println(bottomMessage);
    }

    private void clrscr(){
        try {
            if (System.getProperty("os.name").contains("Windows"))
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            else
                Runtime.getRuntime().exec("clear");
        } catch (IOException | InterruptedException ignored) {}

    }


    private int getNumber(String prompt, int min, int max) throws OutbundInputException {
        System.out.print(prompt);

        int number = scanner.nextInt();
        if (min > number || number > max)
            throw new OutbundInputException("Input value must be from 1 to " + max + "!");

        return number;
    }

    private String[] shufflePlayers(String[] participants) {

        List<String> list = Arrays.asList(participants);
        Collections.shuffle(list);

        return list.toArray(participants);
    }

    private void checkConflict(int x, int y) throws ConflictInputException {
        if ( nonNull(matrix[y][x])) {
            throw new ConflictInputException("Input cell["+x+","+y+"] is already marked!");
        }
    }

    private boolean checkWin(String playerChar, int x, int y) {
        // check horizontal crossing
        if (checkCells(playerChar, MATRIX_LENGTH, i -> new Integer[]{i, y}))
            return true;

        // check vertical crossing
        if (checkCells(playerChar, MATRIX_LENGTH, i -> new Integer[]{x, i}))
            return true;

        // if in the middle - check middle
        if ( (x+y) == (MATRIX_LENGTH-1) ) {
            return checkCells(playerChar, MATRIX_LENGTH, i -> new Integer[]{MATRIX_LENGTH-1-i, i});
        } else if ( x == y ) {
            return checkCells(playerChar, MATRIX_LENGTH, i -> new Integer[]{i, i});
        }

        return false;
    }

    private boolean checkCells(String playerChar, int arrayLength, Function<Integer, Integer[]> funcGetDots) {
        for (int i = 0; i < arrayLength; i++) {
            Integer[] xy = funcGetDots.apply(i);
            int x = xy[0];
            int y = xy[1];

            if ( !(nonNull(matrix[y][x]) && matrix[y][x].equals(playerChar)) )
                return false;

            addWinnerLine(i, x, y);
        }

        return true;
    }

    private void addWinnerLine(int el, int x, int y) {
        winnerLine[el][0] = x;
        winnerLine[el][1] = y;
    }
}
