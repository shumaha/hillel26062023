package hw_21;

public class FileCountLinesHandler extends FileHandler implements Runnable{

    public FileCountLinesHandler(String pathFileToRead, String pathFileToWrite) {
        super(pathFileToRead, pathFileToWrite);
    }

    @Override
    public String handleFileContent() {
        String[] words = getFileContent().split(System.lineSeparator());

        return "Count of lines: " + words.length;
    }

    @Override
    public void run() {
        writeResult(handleFileContent());
    }
}
