package hw_33.controller;

import hw_33.dto.OrderCreateRequest;
import hw_33.entity.Order;
import hw_33.service.OrderService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/orders")
@AllArgsConstructor
public class OrdersController {

    private final OrderService orderService;
    private final Logger logger =  LoggerFactory.getLogger(OrdersController.class);


    @GetMapping("/show")
    public List<Order> showOrders() {
        logger.info("Request /orders/show");

        return orderService.showAll();
    }

    @GetMapping("/show/{id}")
    public Order showOrder(@PathVariable("id") int idOrder) {
        logger.info("Request /orders/show/"+idOrder);

        return orderService.findOrderById(idOrder);
    }

    @GetMapping("/delete/{id}")
    public void deleteOrder(@PathVariable("id") int idOrder) {
        logger.info("Request /orders/delete/"+idOrder);
        orderService.deleteOrderById(idOrder);
    }

    @PostMapping("/create")
    public Order createOrder(@RequestBody OrderCreateRequest createRequest) {
        logger.info("Request /orders/create. Input data: " + createRequest);

        return orderService.addOrder(createRequest);
    }
}
