package hw_20;

import java.io.*;

public class Card implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    private final String number;
    private final String validDate;
    private final String holderName;
    private transient String cvv;
    private transient String pin;


    public Card(String number, String validDate, String holderName, String cvv, String pin) {
        System.out.println("constructor is invoked");
        this.number = number;
        this.validDate = validDate;
        this.holderName = holderName;
        this.cvv = cvv;
        this.pin = pin;
    }

    public String getNumber() {
        return number;
    }

    public String getValidDate() {
        return validDate;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public boolean checkCvv(String cvvForCheck) {

        return cvv.equals(cvvForCheck);
    }

    public boolean checkPin(String pinForCheck) {

        return pin.equals(pinForCheck);
    }

    @Serial
    private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.defaultWriteObject();
        oos.writeObject(this.cvv);
        oos.writeObject(this.pin);
    }

    @Serial
    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        ois.defaultReadObject();
        this.cvv = (String) ois.readObject();
        this.pin = (String) ois.readObject();
    }

    @Override
    public String toString() {
        return "Card{" +
                "number='" + number + '\'' +
                ", validDate='" + validDate + '\'' +
                ", holderName='" + holderName + '\'' +
                ", cvv='" + cvv + '\'' +
                ", pin='" + pin + '\'' +
                '}';
    }
}
