package hw_14.p5;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class App {
    public static void main(String[] args) {
        List<Product> productList = generateProductList();

        System.out.println("Original list: " + productList);
        System.out.println("Grouped map:   " + groupProductsByType(productList));
    }

    private static List<Product> generateProductList() {
        List<Product> generatedList = new ArrayList<>();

        generatedList.add(new Product(Product.Type.Book, 50, LocalDate.parse("2023-08-26")));
        generatedList.add(new Product(Product.Type.Toy, 300, LocalDate.parse("2022-06-03")));
        generatedList.add(new Product(Product.Type.Toy, 78, LocalDate.parse("2020-04-24")));
        generatedList.add(new Product(Product.Type.Book, 68, LocalDate.parse("2019-07-17")));

        Product discountedProduct1 = new Product(Product.Type.Toy, 150); discountedProduct1.enableDiscount(10);
        generatedList.add(discountedProduct1);

        Product discountedProduct2 = new Product(Product.Type.Toy, 46, LocalDate.parse("2023-01-15")); discountedProduct2.enableDiscount(40);
        generatedList.add(discountedProduct2);

        Product discountedProduct3 = new Product(Product.Type.Book, 80, LocalDate.parse("2023-02-13")); discountedProduct3.enableDiscount(10);
        generatedList.add(discountedProduct3);


        return generatedList;
    }

    private static Map<Product.Type, List<Product>> groupProductsByType(List<Product> inList) {

        return inList.stream()
                .collect(Collectors.groupingBy(Product::getType));
    }


}
