package hw_13;

import java.util.Objects;

public class Order {

    private final String customerName;
    private final int orderNumber;

    public Order(String customerName, int orderNumber) {
        this.customerName = customerName;
        this.orderNumber = orderNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public int getOrderNumber() {
        return orderNumber;
    }


    @Override
    public boolean equals(Object o) {

        return o.equals(orderNumber);
    }

    @Override
    public String toString() {
        return "Order{" +
                "customerName='" + customerName + '\'' +
                ", orderNumber=" + orderNumber +
                '}';
    }
}
