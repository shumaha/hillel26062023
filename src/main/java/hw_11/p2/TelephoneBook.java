package hw_11.p2;

import java.util.*;

import static java.util.Objects.isNull;

public class TelephoneBook {

    private final Map<String, Set<TelephoneRecord>> storage = new HashMap<>();

    public void add(TelephoneRecord record) {
        Set<TelephoneRecord> savedSet = storage.get(record.getName());
        if (isNull(savedSet))
            savedSet = new HashSet<>();

        savedSet.add(record);
        storage.put(record.getName(), savedSet);
    }

    public TelephoneRecord find(String name) {
        if (isNull(name) || name.isEmpty())
            return null;

        Set<TelephoneRecord> savedSet = storage.get(name);
        if (isNull(savedSet))
            return null;

        Iterator<TelephoneRecord> iterator = savedSet.iterator();

        return iterator.next();
    }

    public List<TelephoneRecord> findAll(String name) {
        if (isNull(name) || name.isEmpty())
            return null;

        Set<TelephoneRecord> savedSet = storage.get(name);
        if (isNull(savedSet))
            return null;

        return new ArrayList<>(savedSet);
    }
}
