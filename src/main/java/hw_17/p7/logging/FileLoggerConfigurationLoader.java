package hw_17.p7.logging;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;

import static java.util.Objects.isNull;

public class FileLoggerConfigurationLoader {
    private final String confFileName;

    public FileLoggerConfigurationLoader(String confFileName) {
        this.confFileName = confFileName;
    }

    public FileLoggerConfiguration load() {

        try {
            File file = new File(confFileName);
            InputStream inputStream = new BufferedInputStream(new FileInputStream(file), 5);
            byte[] bytes = inputStream.readAllBytes();

            return parseConfiguration(new String(bytes, StandardCharsets.UTF_8));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private FileLoggerConfiguration parseConfiguration(String text) {
        String[] needParams = {"FILE", "LEVEL", "MAX-SIZE", "FORMAT"};

        String[] rows = text.split("\n");
        Map<String, String> params = new LinkedHashMap<>();


        // add all finded params
        for (String row : rows) {
            if (row.trim().isEmpty()) // empty line
                continue;

            String[] parts = row.split(": ", 2);
            params.put(parts[0].trim(), parts[1].trim());
        }

        // check if no exist
        for (String param : needParams) {
            if (isNull(params.get(param))) {
                throw new RuntimeException("Param " + param + " is not exist in conf file (" + confFileName + ")");
            }
        }


        try {
            String file = params.get("FILE");
            LogLevel level = LogLevel.valueOf(params.get("LEVEL"));
            long maxSize = Long.parseLong(params.get("MAX-SIZE"));
            String format = params.get("FORMAT");

            return new FileLoggerConfiguration(
                    file,
                    level,
                    maxSize,
                    format
            );

        } catch (IllegalArgumentException e) {
            throw new RuntimeException("Some param has wrong value! " + e.getMessage());
        }
    }
}
