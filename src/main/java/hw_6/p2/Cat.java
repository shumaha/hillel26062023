package hw_6.p2;

public class Cat extends Participant {

    public Cat(String name, int maxRun, int maxJump, int maxJumpCount) {
        super(name, maxRun, maxJump, maxJumpCount);
    }

    @Override
    public String getLabel() {
        return "~%";
    }


    @Override
    public void run() {
    }

    @Override
    public void jump() {
        super.jump();
    }
}
