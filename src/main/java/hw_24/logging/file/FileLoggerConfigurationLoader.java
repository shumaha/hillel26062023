package hw_24.logging.file;

import hw_24.logging.common.LogLevel;
import hw_24.logging.common.LogerConfigLoader;
import hw_24.logging.common.LoggerConfiguration;

import java.io.*;
import java.util.Properties;
import java.util.UUID;

public class FileLoggerConfigurationLoader extends LogerConfigLoader {

    public FileLoggerConfiguration load(String resource) throws IOException {
        Properties properties = new Properties();
        try (FileReader reader = new FileReader(resource)) {
            properties.load(reader);

            String loggingLevel = properties.getProperty("level", LoggerConfiguration.DEFAULT_LEVEL.name());
            String loggingTimeFormat = properties.getProperty("time.format", LoggerConfiguration.DEFAULT_DATE_FORMAT);
            String loggingFileName = properties.getProperty("file.name", UUID.randomUUID().toString().concat(FileLoggerConfiguration.FILE_EXT));
            int loggingFileSize = Integer.parseInt(properties.getProperty("file.size", Integer.toString(FileLoggerConfiguration.DEFAULT_FILE_SIZE)));
            String loggingMessageFormat = properties.getProperty("message.format", LoggerConfiguration.DEFAULT_MESSAGE_FORMAT);

            return new FileLoggerConfiguration(loggingFileName, loggingFileSize, LogLevel.valueOf(loggingLevel.toUpperCase()), loggingMessageFormat, loggingTimeFormat);
        }
    }
}
