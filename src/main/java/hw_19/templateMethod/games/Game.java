package hw_19.templateMethod.games;

public abstract class Game {
    private final int playersAmount;
    protected Game(int playersAmount) {
        this.playersAmount = playersAmount;
    }
    protected abstract void initGame();
    protected abstract void playGame();
    protected abstract void endGame();
    protected abstract void printWinner();
    public final void startGame() {
        initGame();
        playGame();
        endGame();
        printWinner();
    }
}
