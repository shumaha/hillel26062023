package hw_14.p2;

public class Product {

    final private Type type;
    private double price;

    private boolean discountEnable = false;

    private int discountValue = 0;

    public Product(Type type, int price) {
        this.type = type;
        this.price = price;
    }

    public Type getType() {
        return type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public double getFinalPrice() {

        return discountEnable ? price - (price * discountValue/100) : price;
    }

    public int getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(int discountValue) {
        if (!discountEnable) {
            throw new IllegalArgumentException("Discount is not enabled!");
        }

        if (1 > discountValue || discountValue > 99) {
            throw new IllegalArgumentException("Discount value can be from 1 to 99%!");
        }

        this.discountValue = discountValue;
    }

    public void enableDiscount(int discountValue) {
        if (1 > discountValue || discountValue > 99) {
            throw new IllegalArgumentException("Discount value can be from 1 to 99%!");
        }

        this.discountEnable = true;
        this.discountValue = discountValue;
    }

    public void disableDiscount() {
        this.discountEnable = false;
        this.discountValue = 0;
    }

    public boolean isDiscountEnable() {
        return discountEnable;
    }

    public enum Type {
        Book,
        Toy
    }

    @Override
    public String toString() {
        return "Product{" +
                "type=" + type +
                ", price=" + getFinalPrice() +
                (discountEnable ? ", discount: "+discountEnable+" ("+discountValue +  "% off from "+price+")" : "") +
                '}';
    }
}