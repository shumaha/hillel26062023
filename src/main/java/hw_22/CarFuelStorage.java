package hw_22;

public class CarFuelStorage extends FuelStorage {
    public CarFuelStorage(int maxCapacity) {
        super(maxCapacity);
    }

    public CarFuelStorage(int maxCapacity, int currentCapacity) {
        super(maxCapacity, currentCapacity);
    }
}
