package hw_4;

public class Cat implements Animal {
    private String name;

    public Cat(String name) {
        this.name = name;
    }

    public void run(int meters) {
        System.out.println(name + " пробіг " + (meters > 200 ? 200:meters) + " з потрібних " + meters + " м.");
    }

    @Override
    public void swim(int meters) {
        System.out.println(name + " не вміє плавати - у нього лапки :(");
    }

}
