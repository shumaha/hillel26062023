package hw_19.templateMethod.games;

public abstract class TwoPersonGame extends Game {

    public TwoPersonGame() {
        super(2);
    }
}
