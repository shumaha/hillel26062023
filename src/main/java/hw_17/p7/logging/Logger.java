package hw_17.p7.logging;

public interface Logger {
    void info(String message);
    void debug(String message);
    void close();
}
