package hw_22;

public class Car {
    private final String model;
    private final String plateNumber;
    private final FuelStorage fuelStorage;

    public Car(String model, String plateNumber, int maxFuelStorageCapacity, int currentFuelStorageCapacity) {
        this.model = model;
        this.plateNumber = plateNumber;
        this.fuelStorage = new CarFuelStorage(maxFuelStorageCapacity, currentFuelStorageCapacity);
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", plateNumber='" + plateNumber + '\'' +
                ", fuelStorage(cur/max)=" + fuelStorage.getCurrentCapacity() + "/" + fuelStorage.getMaxCapacity() +
                '}';
    }

    public FuelStorage getFuelStorage() {
        return fuelStorage;
    }
}
