package hw_19.abstractFactory.factories;

public class YoutubeMusic extends Music {
    protected YoutubeMusic() {
        super("Youtube Music");
    }
}
