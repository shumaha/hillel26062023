package hw_9.p1;

import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) {
        //String[] arrayToConvert = {"one", "two", "three"};
        Float[] arrayToConvert = {3.14f, 34.3f, 499.1f};

        System.out.println("List<> from array:");
        for (Object s: convertArrayToList(arrayToConvert)) {
            System.out.println("element (type="+s.getClass().getSimpleName()+"): " + s.toString());
        }

        convertArrayToList(arrayToConvert);

    }

    public static <T> List<T> convertArrayToList(T[] array) {
        List<T> list = new ArrayList<>();
        for (T element: array)
            list.add(element);

        return list;
    }
}

