package hw_3;

public class BurgerMain {

    public static void main(String[] args) {

        System.out.println("--- dietary Burger ---");
        Burger dietaryBurger = new Burger(false, false);

        System.out.println("--- main Burger ---");
        Burger mainBurger = new Burger();

        System.out.println("--- doubleMeat Burger ---");
        Burger doubleMeatBurger = new Burger(true);
    }
}
