package hw_24.logging.common;

public abstract class Logger {
    public abstract void info(String message);
    public abstract void debug(String message);
}
