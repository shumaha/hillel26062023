package hw_14.p1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class App {
    public static void main(String[] args) {
        List<Product> productList = new ArrayList<>(Arrays.asList(
                new Product(Product.Type.Book, 100),
                new Product(Product.Type.Book, 256),
                new Product(Product.Type.Toy, 300),
                new Product(Product.Type.Toy, 78),
                new Product(Product.Type.Book, 356),
                new Product(Product.Type.Toy, 700)
        ));

        System.out.println("Original list: " + productList);
        System.out.println("Filtered list:  " + filterList(productList));

    }

    private static List<Product> filterList(List<Product> inList) {

        return inList.stream()
                .filter(product -> product.getType() == Product.Type.Book && product.getPrice() > 250)
                .collect(Collectors.toList());
    }


}
