package hw_32.repository;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Component
public final class DataBaseConnection {

    private static Connection connection;

    @Value("${db.ip}")
    private String ip;
    @Value("${db.port}")
    private int port;
    @Value("${db.username}")
    private String username;
    @Value("${db.password}")
    private String password;
    @Value("${db.database}")
    private String database;

    public synchronized Connection getConnection() throws SQLException {

        if (connection == null || connection.isClosed()) {
            connection = DriverManager.getConnection("jdbc:mysql://"+ip+":"+port+"/"+database, username, password);
        }

        return connection;
    }

    public synchronized void closeConnection(Connection conn) throws SQLException {
        conn.close();
    }
}
