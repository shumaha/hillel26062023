package hw_32.service;

import hw_32.entity.Product;
import hw_32.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<Product> showAll() {

        return productRepository.selectAll();

    }

    public Product findProductById(int id) {
        Optional<Product> optionalProduct = productRepository.findProductById(id);

        return optionalProduct.orElse(null);


    }

}
