package hw_14.p5;

public class ProductIdGenerator {

    private static int nextId = 1;

    public static int getNextId() {
        return nextId++;
    }
}
