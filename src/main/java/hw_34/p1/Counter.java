package hw_34.p1;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Counter {
    private int count;
    private final Lock lock = new ReentrantLock();

    public void increment() {
        if (lock.tryLock()) {
            try {
                System.out.println("count++: " + (++count));
            } finally {
                lock.unlock();
            }
        } else {
            System.out.println("count++ else");
        }
        sleep(100);
    }

    public void decrement() {
        if (lock.tryLock()) {
            try {
                System.out.println("count--: " + (--count));
            } finally {
                lock.unlock();
            }
        } else {
            System.out.println("count-- else");

        }
        sleep(100);
    }

    private void sleep(long timeout) {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString() {
        return "Counter{" +
                "count=" + count +
                '}';
    }
}
