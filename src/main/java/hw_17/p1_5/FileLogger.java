package hw_17.p1_5;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class FileLogger {
    private final FileLoggerConfiguration conf;
    private final OutputStream outputStream;
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private long writedBytesCount;

    public FileLogger(FileLoggerConfiguration conf) {
        this.conf = conf;
        File file = new File(conf.getFileName());
        writedBytesCount = file.length();

        try {
            outputStream =
                    new BufferedOutputStream(
                            new FileOutputStream(file, true), 50);
        }
        catch (IOException e) {
            throw new RuntimeException("Can't create stream for writing log to file!");
        }
    }

    public void info(String message) throws FileMaxSizeReachedException {
        if (canLog(LogLevel.INFO)) {
            writeMessage(templateMessage(message, LogLevel.INFO));
        }
    }

    public void debug(String message) throws FileMaxSizeReachedException {
        if (canLog(LogLevel.DEBUG)) {
            writeMessage(templateMessage(message, LogLevel.DEBUG));
        }
    }

    private void writeMessage(String message) throws FileMaxSizeReachedException {
        byte[] bytes = message.getBytes(StandardCharsets.UTF_8);

        if ( (writedBytesCount+bytes.length) >= conf.getMaxSize()) {
            try {
                outputStream.flush();
            } catch (IOException ignored) {}

            throw new FileMaxSizeReachedException("Can't write next message! The maximum size (" + conf.getMaxSize() + " bytes) of file (" + conf.getFileName() + ") will be exceed! Current file size is " + writedBytesCount + " bytes.");
        }

        writedBytesCount += bytes.length;

        try {
            outputStream.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean canLog(LogLevel level) {

        return level.value >= conf.getLevel().value;
    }

    private String templateMessage(String message, LogLevel level) {
        String templetedStr = conf.getFormat();
        templetedStr = templetedStr.replace("[CURRENT_TIME]", LocalDateTime.now().format(formatter));
        templetedStr = templetedStr.replace("[LEVEL]", level.name());
        templetedStr = templetedStr.replace("[MESSAGE_STRING]", message);

        return templetedStr + "\n";
    }

    public void close() {
        try {
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
