package hw_11.p2;

import java.util.List;

public class App {
    public static void main(String[] args) {
        TelephoneBook book = new TelephoneBook();
        book.add(new TelephoneRecord("Игорь", "911"));
        book.add(new TelephoneRecord("Игорь", "12345678"));
        System.out.println("find(): " + book.find("Игорь").getNumber());

        List<TelephoneRecord> findedNumbers = book.findAll("Игорь");
        System.out.println("\nfindAll(): ");
        if (findedNumbers == null)
            System.out.println("Nothing is found");
        else {
            for (TelephoneRecord rec : findedNumbers)
                System.out.println(rec.getName() + ": " + rec.getNumber());
        }
    }
}
