package hw_23;

public class ValueCalculator {

    private final float[] numberArray;
    private final int size;
    private final int half;


    public ValueCalculator(int size) {
        if (size%2 != 0)
            throw new IllegalArgumentException("Input size "+size+" is wrong! Size must be even number!");
        if (size < 1_000_000)
            throw new IllegalArgumentException("Input size "+size+" is wrong! Size must be >= 1 000 000!");

        this.size = size;
        this.half = size/2;
        this.numberArray = new float[size];
    }

    public void calculate() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < size; i++) {
            numberArray[i] = 7;
        }

        float[] a1 = new float[half];
        float[] a2 = new float[half];

        System.arraycopy(numberArray, 0 , a1, 0, half);
        System.arraycopy(numberArray, half , a2, 0, half);

        Thread calculateRunner1 = new Thread(new CalculateRunner(a1));
        Thread calculateRunner2 = new Thread(new CalculateRunner(a2));

        try {
            calculateRunner1.start();
            calculateRunner1.join();
            calculateRunner2.start();
            calculateRunner2.join();
        } catch (InterruptedException ingored) {}

        System.arraycopy(a1, 0, numberArray, 0, half);
        System.arraycopy(a2, 0, numberArray, half, half);

        long end = System.currentTimeMillis();
        System.out.println("Calculate time spended: " + (end - start) + " ms.");
    }

    private class CalculateRunner implements Runnable {
        private final float[] arr;

        private CalculateRunner(float[] arr) {
            this.arr = arr;
        }

        @Override
        public void run() {
            for (int i = 0; i < arr.length; i++) {
                arr[i] = (float) (arr[i] * Math.sin(0.2f + i / 5) * Math.cos(0.2f + i / 5) * Math.cos(0.4f + i / 2));
            }
        }
    }
}
