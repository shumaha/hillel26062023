package hw_10;

import java.util.*;

import static java.util.Objects.isNull;

public class AnotherList<E> implements List<E> {

    private Object[] storageArray;
    private int size = 0;

    private final int DEFAULT_SIZE = 10;
    private final Object[] DEFAULT_VOID_ARRAY = {};

    private final double GROW_FACTOR = 1.5;

    public AnotherList() {
        this.storageArray = DEFAULT_VOID_ARRAY;
    }

    public AnotherList(int size) {
        this.storageArray = new Object[size];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        if (o == null) {
            for (int i = 0; i < size; i++) {
                if (storageArray[i] == null)
                    return true;
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (storageArray[i].equals(o))
                    return true;
            }
        }

        return false;
    }

    @Override
    public Iterator<E> iterator() {
        return new IteratorImpl();
    }

    private class IteratorImpl implements Iterator<E> {
        private int position = 0;

        @Override
        public boolean hasNext() {
            return position < size;
        }

        @SuppressWarnings("unchecked")
        @Override
        public E next() {
            return (E) storageArray[position++];
        }
    }

    @Override
    public Object[] toArray() {
        return Arrays.copyOf(storageArray, size);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T[] toArray(T[] a) {
        if (a.length < size)
            return (T[]) Arrays.copyOf(storageArray, size, a.getClass());

        System.arraycopy(storageArray, 0, a, 0, size);
        if (a.length > size)
            a[size] = null;

        return a;
    }

    @Override
    public boolean add(E e) {
        if (size == storageArray.length)
            storageArray = grow();

        storageArray[size] = e;
        size++;

        return true;
    }

    @Override
    public void add(int index, E element) {
        Objects.checkIndex(index, size);

        if (size == storageArray.length)
            storageArray = grow();

        System.arraycopy(storageArray, index, storageArray, index+1, size-index);
        storageArray[index] = element;
        size++;
    }

    private Object[] grow(int minimumGrow) {
        if (size == 0) {
            return new Object[DEFAULT_SIZE];
        } else {
            
            int preferedCapacity = Math.max( (int) (size * GROW_FACTOR), size+minimumGrow); // prefered next array size

            // overflow
            if (preferedCapacity < 0 && size < Integer.MAX_VALUE) {
                preferedCapacity = Integer.MAX_VALUE;
            } else if (preferedCapacity < 0 && size == Integer.MAX_VALUE) {
                throw new OutOfMemoryError("Size MAX length of List is reached!");
            }

            return Arrays.copyOf(storageArray, preferedCapacity);
        }
    }
    
    private Object[] grow() {
        return grow(size+1);
    }

    @Override
    public boolean remove(Object o) {
        boolean isFound = false;
        int i = 0;
        if (o == null) {
            for (; i < size; i++) {
                if (storageArray[i] == null) {
                    isFound = true;
                    break;
                }
            }
        } else {
            for (; i < size; i++) {
                if (storageArray[i].equals(o)) {
                    isFound = true;
                    break;
                }
            }
        }

        removeInternal(i);

        return isFound;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        if (c == null)
            throw new NullPointerException("Collection is NULL!");

        Object[] objects = c.toArray();
        for (Object o: objects) {
            if (!contains(o))
                return false;
        }

        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        if (c == null)
            throw new NullPointerException("Collection is NULL!");

        if (!isInoughSpaceToAdd(c.size()) || c.isEmpty())
            return false;

        Object[] objToAdd = c.toArray();
        if (storageArray.length-size < objToAdd.length)
            storageArray = grow(objToAdd.length);

        System.arraycopy(objToAdd, 0, storageArray, size, objToAdd.length);
        size += objToAdd.length;

        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        Objects.checkIndex(index, size);

        if (!isInoughSpaceToAdd(c.size()) || c.isEmpty())
            return false;

        Object[] objToAdd = c.toArray();

        int cntToMove = size-index;
        int cntToAdd = objToAdd.length;
        if (storageArray.length-size < objToAdd.length)
            storageArray = grow(objToAdd.length);

        System.arraycopy(storageArray, index, storageArray, index+cntToAdd, cntToMove);
        System.arraycopy(objToAdd, 0, storageArray, index, objToAdd.length);
        size += cntToAdd;

        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        if (c == null)
            throw new NullPointerException("Collection is NULL!");

        boolean result = false;

        int newCnt=0;
        for (int i = 0; i < size; i++) {
            Object o;
            if (!c.contains(o = storageArray[i])) {
                storageArray[newCnt++] = o;
            } else
                result = true;
        }

        trimStorageToNewSize(size-newCnt);

        return result;
    }

    private void trimStorageToNewSize(int trimCnt) {

        for (int i = size-1,j=0; j<trimCnt; i--,j++) {
            storageArray[i] = null;
        }
        size -= trimCnt;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        if (c == null)
            throw new NullPointerException("Collection is NULL!");

        boolean result = false;

        int newCnt=0;
        for (int i = 0; i < size; i++) {
            Object o;
            if (c.contains(o = storageArray[i])) {
                storageArray[newCnt++] = o;
            } else
                result = true;
        }

        trimStorageToNewSize(size-newCnt);

        return result;
    }

    @Override
    public void clear() {
        size = 0;
        storageArray = DEFAULT_VOID_ARRAY;
    }

    @SuppressWarnings("unchecked")
    @Override
    public E get(int index) {
        Objects.checkIndex(index, size);
        return (E) storageArray[index];
    }

    @Override
    public E set(int index, E element) {
        E prev = get(index);
        storageArray[index] = element;

        return prev;
    }

    @SuppressWarnings("unchecked")
    @Override
    public E remove(int index) {
        Objects.checkIndex(index, size);

        E removedElement = (E) storageArray[index];
        removeInternal(index);

        return removedElement;
    }

    private void removeInternal(int index) {
        int cntToMove = --size - index;

        if (cntToMove > 0)
            System.arraycopy(storageArray, index+1, storageArray, index, cntToMove);

        storageArray[size] = null;
    }

    @Override
    public int indexOf(Object o) {

        if (o == null) {
            for (int i = 0; i < size; i++) {
                if (isNull(storageArray[i]))
                    return i;
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (storageArray[i].equals(o))
                    return i;
            }
        }

        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<E> listIterator() {
        return new ListIteratorImpl();
    }

    private class ListIteratorImpl implements ListIterator<E> {

        @Override
        public boolean hasNext() {
            return false;
        }

        @Override
        public E next() {
            return null;
        }

        @Override
        public boolean hasPrevious() {
            return false;
        }

        @Override
        public E previous() {
            return null;
        }

        @Override
        public int nextIndex() {
            return 0;
        }

        @Override
        public int previousIndex() {
            return 0;
        }

        @Override
        public void remove() {

        }

        @Override
        public void set(E e) {
        }

        @Override
        public void add(E e) {

        }
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return null;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return null;
    }
    
    private boolean isInoughSpaceToAdd(int elementToAdd) {
        if (Integer.MAX_VALUE-size < elementToAdd)
            return false;

        return true;

    }

    public String toString() {
        StringBuilder builder = new StringBuilder(this.getClass().getSimpleName() +"["+size()+"]: {");
        for (int i = 0; i < size; i++) {
            builder.append(storageArray[i].toString()).append(',');
        }

        if (!isEmpty())
            builder.deleteCharAt(builder.length()-1);

        return builder.append("}").toString();
    }
}
