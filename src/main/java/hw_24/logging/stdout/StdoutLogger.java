package hw_24.logging.stdout;

import hw_24.logging.common.LogLevel;
import hw_24.logging.common.Logger;

import java.io.IOException;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static java.util.Objects.nonNull;

public class StdoutLogger extends Logger {
    private final StdoutLoggerConfiguration conf;
    private final DateTimeFormatter dateFormatter;
    private final String formatPattern;
    private final Class<?> clazz;
    private final String ansiColorCode;

    public StdoutLogger(Class<?> clazz) throws IOException {
        this.conf = new StdoutLoggerConfigurationLoader().load("logging.properties");
        this.clazz = clazz;
        this.dateFormatter = DateTimeFormatter.ofPattern(conf.getCurrentDateFormat());
        this.formatPattern = generateFormatPattern(conf.getCurrentMessageFormat());

        if (nonNull(conf.getColor())) {
            this.ansiColorCode = Color.valueOf(conf.getColor().toUpperCase()).get();
        } else {
            ansiColorCode = null;
        }
    }

    @Override
    public void info(String message) {
        if (canLog(LogLevel.INFO)) {
            writeMessage(templateMessage(message, LogLevel.INFO));
        }
    }

    @Override
    public void debug(String message) {
        if (canLog(LogLevel.DEBUG)) {
            writeMessage(templateMessage(message, LogLevel.DEBUG));
        }
    }

    private void writeMessage(String message) {
        System.out.println(message);
    }

    private boolean canLog(LogLevel level) {

        return level.value >= conf.getLevel().value;
    }
    private String templateMessage(String message, LogLevel level) {
        String templetedStr = MessageFormat.format
                (formatPattern,
                        LocalDateTime.now().format(dateFormatter),
                        clazz.getName(),
                        message,
                        level.name()
                );

        if (nonNull(ansiColorCode)) {
            String ansiReset = "\u001B[0m";
            templetedStr = ansiColorCode + templetedStr + ansiReset;
        }

        return templetedStr;
    }

    public String generateFormatPattern(String propertyFormat) {
        String[] propList = new String[]{"time", "className", "message","level"};

        String templetedStr = propertyFormat;
        for (int i = 0; i < propList.length; i++) {
            templetedStr = templetedStr.replaceAll("(?i)" + "\\{"+propList[i]+"}", "{"+i+"}");
        }

        return templetedStr;
    }
}
