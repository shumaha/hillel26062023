package hw_17.p1_5;

import java.io.*;

public class App {
    public static void main(String[] args) throws IOException {
        FileLoggerConfiguration fileLoggerConfiguration;
//        fileLoggerConfiguration = new FileLoggerConfiguration("SingleLogFile.txt", LogLevel.INFO, 1024, "[CURRENT_TIME]\t[LEVEL]\tПовідомлення: [MESSAGE_STRING]");
        fileLoggerConfiguration = new FileLoggerConfigurationLoader("FileLogger.conf").load();


        FileLogger logger = new FileLogger(fileLoggerConfiguration);


        // log messages
        try {
            logger.info("Warning message");
            logger.debug("Error message");
        } catch (FileMaxSizeReachedException e) {
            e.printStackTrace();
        }

        logger.close();
    }
}
