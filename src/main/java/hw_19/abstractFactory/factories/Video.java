package hw_19.abstractFactory.factories;

public abstract class Video {

    private final String type;


    protected Video(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Video{" +
                "type='" + type + '\'' +
                '}';
    }
}
