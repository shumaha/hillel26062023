package hw_16;

import hw_16.annotations.AfterSuite;
import hw_16.annotations.BeforeSuite;
import hw_16.annotations.Test;

public class ClassForTest {

    @Deprecated
    public void make() {
        System.out.println("Method make()");
    }

    @AfterSuite
    public void fly() {
        System.out.println("Method fly()");
    }

    @Test(1)
    public void swim() {
        System.out.println("Method swim()");
    }

    @BeforeSuite
    public void sit() {
        System.out.println("Method sit()");
    }

    @Test
    public void walk() {
        System.out.println("Method walk()");
    }
}
