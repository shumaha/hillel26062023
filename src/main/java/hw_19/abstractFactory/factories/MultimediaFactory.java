package hw_19.abstractFactory.factories;

public interface MultimediaFactory {

    Video generateVideo();
    Music generateMusic();
}
