package hw_18.decorator;

import hw_18.decorator.decorators.SqueezeSpaceDecorator;
import hw_18.decorator.decorators.UpperCaseSQLDecorator;
import hw_18.decorator.interfaces.SQLRequestFormatter;

public class App {
    public static void main(String[] args) {
        String sqlRequest = "\t       \n  select top 100 from\tshop..orders; "
                +"\nupdate      shop..orders set shippingDate = getdate() where id      =  5823;  \n    ";

        System.out.println("Input string: \"" + sqlRequest + "\"");

        SQLRequestFormatter formatter =
                    new SqueezeSpaceDecorator(
                        new UpperCaseSQLDecorator(
                            new TrimSQLFormatter()));

        System.out.println("Output string: \"" + formatter.format(sqlRequest) + "\"");
    }
}
