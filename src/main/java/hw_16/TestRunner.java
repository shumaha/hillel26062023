package hw_16;

import hw_16.annotations.AfterSuite;
import hw_16.annotations.BeforeSuite;
import hw_16.annotations.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

import static java.util.Objects.isNull;

public class TestRunner {

    public static void start(Class<?> clazz) {

        Map<String, DataSaver> annotationsMap = new LinkedHashMap<>();
        Map<Method, Integer> testMethods = new LinkedHashMap<>();

        // add all valid annotations with methods
        for (Method method : clazz.getDeclaredMethods()) {

            for (Annotation annotation: method.getDeclaredAnnotations()) {
                String validAnnotationName = testAndGetName(annotation);

                if (!isNull(validAnnotationName)) {
                    if (annotationsMap.putIfAbsent(validAnnotationName, new DataSaver(method)) != null) {
                        annotationsMap.get(validAnnotationName).addMethod(method);
                    }

                    if (validAnnotationName.equals("Test")) {
                        testMethods.put(method, getTestAnnotationValue(annotation));
                    }
                }
            }
        }

        checkRestrictions(clazz, annotationsMap, testMethods);

        // Generate instance for runing methods
        Object object = generateInstance(clazz);

        // run @BeforeSuite method if exist
        if (annotationsMap.get("BeforeSuite") != null) {
            invokeMethod(annotationsMap.get("BeforeSuite").methods.get(0), object);
        }

        // run @Test methods
        testMethods.entrySet().stream()
                .sorted(Collections.reverseOrder(Comparator.comparing(Map.Entry::getValue)))
                .forEach(t-> invokeMethod(t.getKey(), object));

        // run @AfterSuite method
        invokeMethod(annotationsMap.get("AfterSuite").methods.get(0), object);
    }

    private static String testAndGetName(Annotation annotation) {

        if (annotation instanceof BeforeSuite)
            return "BeforeSuite";
        else if (annotation instanceof Test)
            return "Test";
        else if (annotation instanceof AfterSuite)
            return "AfterSuite";

        return null;
    }

    private static int getTestAnnotationValue(Annotation annotation) {
        return ((Test) annotation).value();
    }

    private static void checkRestrictions(Class<?> clazz, Map<String, DataSaver> aMap, Map<Method, Integer> testMethods) {
        // ВАЖЛИВО! Інструкція @Test свідчить у тому, що даним метод є “методом-тестом”.
        if ( isNull(aMap.get("Test")) ) {
            throw new RuntimeException("Class "+clazz.getSimpleName()+" is not test-class! @Test methods are not present!");
        }

        // До кожного методу-тесту необхідно також додати пріоритети (int числа від 1 до 10)
        testMethods.forEach((key, value) -> {
            if (1 > value || value > 10)
                throw new RuntimeException("Class " + clazz.getSimpleName() + ", method [" + key + "] has too much priority! Must be from 1 to 10.");
        });

        // 5. Методи з анотаціями @BeforeSuite та @AfterSuite у межах одного “класу-тесту” повинні бути присутніми в єдиному екземплярі, інакше необхідно викинути виняток.
        if ( !isNull(aMap.get("BeforeSuite")) && aMap.get("BeforeSuite").count > 1) {
            throw new RuntimeException("Class "+clazz.getSimpleName()+". @BeforeSuite method must be the only one!");
        }
        if ( !isNull(aMap.get("AfterSuite")) && aMap.get("AfterSuite").count > 1) {
            throw new RuntimeException("Class "+clazz.getSimpleName()+". @AfterSuite method must be the only one!");
        }
        if ( isNull(aMap.get("AfterSuite"))) {
            throw new RuntimeException("Class "+clazz.getSimpleName()+". @AfterSuite method are not present!");
        }
    }

    public static void invokeMethod(Method methodToRun, Object obj) {
        try {
            methodToRun.invoke(obj);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public static Object generateInstance(Class<?> clazz) {

        try {
            Constructor<?> constructor = clazz.getConstructor();
            Object obj = constructor.newInstance();

            return obj;

        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    private static class DataSaver {
        final private List<Method> methods = new ArrayList<>();
        private int count = 1;

        public DataSaver(Method methodName) {
            methods.add(methodName);
        }

        public void addMethod(Method methodName) {
            methods.add(methodName);
            count++;
        }

        @Override
        public String toString() {
            return "DataSaver{" +
                    "methods=" + methods +
                    ", count=" + count +
                    '}';
        }
    }
}
