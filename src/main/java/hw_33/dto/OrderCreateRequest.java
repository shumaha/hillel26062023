package hw_33.dto;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class OrderCreateRequest {
    private final String date;
    private final List<Integer> products;
}
