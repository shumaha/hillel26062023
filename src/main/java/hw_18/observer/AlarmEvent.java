package hw_18.observer;

import hw_18.observer.interfaces.Event;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class AlarmEvent implements Event {
    private static final String[] regionsArray = new String[]{
            "Одеська",
            "Дніпропетровська",
            "Чернігівська",
            "Харківська",
            "Житомирська",
            "Полтавська",
            "Херсонська",
            "Київська",
            "Запорізька",
            "Луганська",
            "Донецька",
            "Вінницька",
            "Автономна Республіка Крим",
            "Миколаївська",
            "Кіровоградська",
            "Сумська",
            "Львівська",
            "Черкаська",
            "Хмельницька",
            "Волинська",
            "Рівненська",
            "Івано-Франківська",
            "Тернопільська",
            "Закарпатська",
            "Чернівецька"
    };

    private final RegionId[] alarmRegionsIdArray;
    private final boolean isAlarmStart;
    private final LocalDateTime eventTime;

    public AlarmEvent(boolean isAlarmStart, LocalDateTime eventTime,  RegionId... regionIds) {
        if (regionIds.length == 0)
            throw new IllegalArgumentException("Array of IDes can't be empty or null!");

        // check if there is duplicated ID
        Optional<Map.Entry<String, List<RegionId>>> first = Arrays.stream(regionIds)
                .collect(Collectors.groupingBy(RegionId::toString))
                .entrySet().stream().max(Comparator.comparingInt(o -> o.getValue().size()));

        if (first.get().getValue().size() > 1) {
            throw new IllegalArgumentException("RegionId must be unique! ID " + first.get().getKey() + " is duplicated.");
        }

        this.alarmRegionsIdArray = regionIds;
        this.isAlarmStart = isAlarmStart;
        this.eventTime = eventTime;
    }

    public String printRegions() {
        StringBuilder stringBuilder = new StringBuilder();
        for (RegionId regionEnum : alarmRegionsIdArray) {
            stringBuilder.append(regionsArray[regionEnum.id-1]).append(", ");
        }

        String fullStr = stringBuilder.toString();

        return fullStr.substring(0, fullStr.length()-2);
    }

    public RegionId[] getAlarmRegionsIdArray() {
        return alarmRegionsIdArray;
    }

    public boolean isAlarmStart() {
        return isAlarmStart;
    }

    public LocalDateTime getEventTime() {
        return eventTime;
    }

    public enum RegionId {
        ODESKA(1),
        DNIPROPETROVSKA(2),
        CHERNIHIVSKA(3),
        KHARKIVSKA(4),
        ZHYTOMYRSKA(5),
        POLTAVSKA(6),
        KHERSONSKA(7),
        KYIVSKA(8),
        ZAPORIZKA(9),
        LUHANSKA(10),
        DONETSKA(11),
        VINNYTSKA(12),
        ARK(13),
        MYKOLAIVSKA(14),
        KIROVOHRADSKA(15),
        SUMSKA(16),
        LVIVSKA(17),
        CHERKASKA(18),
        KHMELNYTSKA(19),
        VOLYNSKA(20),
        RIVNENSKA(21),
        IVANOFRANKIVSKA(22),
        TERNOPILSKA(23),
        ZAKARPATSKA(24),
        CHERNIVETSKA(25);

        public final int id;

        RegionId(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        @Override
        public String toString() {
            return name() + "(" + id + ")";
        }
    }
}