package hw_19.abstractFactory;

import hw_19.abstractFactory.factories.LocalMultimediaFactory;
import hw_19.abstractFactory.factories.MultimediaPlayer;
import hw_19.abstractFactory.factories.YoutubeMultimediaFactory;

public class App {
    public static void main(String[] args) {
        MultimediaPlayer youtubePlayer = new MultimediaPlayer(new YoutubeMultimediaFactory());
        System.out.println(youtubePlayer.getRandomVideo());
        System.out.println(youtubePlayer.getRandomMusic());


        MultimediaPlayer localPlayer = new MultimediaPlayer(new LocalMultimediaFactory());
        System.out.println(localPlayer.getRandomVideo());
        System.out.println(localPlayer.getRandomMusic());

    }
}
