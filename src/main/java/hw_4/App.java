package hw_4;

public class App {
    public static void main(String[] args) {
        Animal dog1 = new Dog("Бобік");
        Animal dog2 = new Dog("Патрон");
        Animal cat1 = new Cat("Мурзік");

        Animal[] animals = {dog1, cat1, dog2};

        int dogCnt = 0;
        int catCnt = 0;

        for (Animal animal : animals) {
            animal.run(250);
            animal.swim(13);

            if (animal instanceof Dog) {
                dogCnt++;
                continue;
            }

            if (animal instanceof Cat)
                catCnt++;
        }

        System.out.println("\nКількість тварин: " + (dogCnt+catCnt) + ", собак: " + dogCnt + ", котів: " + catCnt);
    }
}
