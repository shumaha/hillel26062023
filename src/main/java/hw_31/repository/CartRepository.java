package hw_31.repository;

import hw_31.entity.Product;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CartRepository {

    private final List<Product> storage;
    private final ProductRepository productRepository;

    public CartRepository(ProductRepository productRepository) {
        this.storage = new ArrayList<>();
        this.productRepository = productRepository;
    }

    public boolean add(UUID uuid) {
        Optional<Product> optionalProduct = productRepository.findById(uuid);
        optionalProduct.ifPresent((product -> storage.add(product)));

        return optionalProduct.isPresent();
    }

    public boolean delete(UUID uuid) {
        Optional<Product> optionalProduct = productRepository.findById(uuid);

        return optionalProduct.map(product -> storage.remove(product)).orElse(false);

    }

    public List<Product> findAll() {

        return new ArrayList<>(storage);
    }

    @Override
    public String toString() {
        return "CardRepository{" +
                "storage=" + storage +
                '}';
    }
}
