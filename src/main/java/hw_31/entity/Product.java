package hw_31.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.util.UUID;

@Data
@AllArgsConstructor
public class Product {
    private final UUID id;
    private final String name;
    private BigDecimal price;
}
