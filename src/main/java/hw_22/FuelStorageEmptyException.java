package hw_22;

public class FuelStorageEmptyException extends Exception {
    public FuelStorageEmptyException(String message) {
        super(message);
    }
}
