package hw_11.p2;

import java.util.Objects;

public class TelephoneRecord {
    private final String name;
    private final String number;

    public TelephoneRecord(String name, String number) {
        if (name == null || name.isEmpty() || number == null || number.isEmpty())
            throw new IllegalArgumentException("One or both arguments can't be null or empty!");

        this.name = name;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TelephoneRecord record = (TelephoneRecord) o;
        return Objects.equals(name, record.name) && Objects.equals(number, record.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, number);
    }
}
