package hw_6.p1;

public class Triangle implements Shape {

    private int sideLength;

    public Triangle(int sideLength) {
        this.sideLength = sideLength;
    }

    @Override
    public double getArea() {
        return (Math.sqrt(3)/4)*Math.pow(sideLength, 2);
    }
}
