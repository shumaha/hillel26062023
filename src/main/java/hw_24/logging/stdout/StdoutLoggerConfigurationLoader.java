package hw_24.logging.stdout;

import hw_24.logging.common.LogLevel;
import hw_24.logging.common.LogerConfigLoader;
import hw_24.logging.common.LoggerConfiguration;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class StdoutLoggerConfigurationLoader extends LogerConfigLoader {

    public StdoutLoggerConfiguration load(String resource) throws IOException {
        Properties properties = new Properties();
        try (FileReader reader = new FileReader(resource)) {
            properties.load(reader);

            String loggingLevel = properties.getProperty("level", LoggerConfiguration.DEFAULT_LEVEL.name());
            String loggingTimeFormat = properties.getProperty("time.format", LoggerConfiguration.DEFAULT_DATE_FORMAT);
            String loggingMessageFormat = properties.getProperty("message.format", LoggerConfiguration.DEFAULT_MESSAGE_FORMAT);
            String color = properties.getProperty("message.color", null);

            return new StdoutLoggerConfiguration(color, loggingMessageFormat, loggingTimeFormat, LogLevel.valueOf(loggingLevel.toUpperCase()));
        }
    }
}
