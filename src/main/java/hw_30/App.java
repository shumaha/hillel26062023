package hw_30;

import hw_30.beans.PostService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

    public static final String CONFIG_PATH = "hw_30.configuration";

    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(CONFIG_PATH);

        Object bankServiceMono = applicationContext.getBean("Monobank");
        Object bankServicePrivat = applicationContext.getBean("Privatbank");
        PostService postServiceNova = applicationContext.getBean(PostService.class);

        System.out.println("Beans:");
        System.out.println("Bean: " + bankServiceMono.getClass().getName() + " - " + bankServiceMono);
        System.out.println("Bean: " + bankServicePrivat.getClass().getName() + " - " + bankServicePrivat);
        System.out.println("Bean: " + postServiceNova.getClass().getName() + " - " + postServiceNova);
    }
}
