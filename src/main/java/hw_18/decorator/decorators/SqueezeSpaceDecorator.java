package hw_18.decorator.decorators;

import hw_18.decorator.interfaces.SQLRequestFormatter;

public class SqueezeSpaceDecorator extends SQLRequestFormatterDecorator {


    public SqueezeSpaceDecorator(SQLRequestFormatter sqlRequestFormatter) {
        super(sqlRequestFormatter);
    }

    @Override
    String decorate(String sqlRequest) {
        return sqlRequest.replaceAll("\\s{2,}|\\s+\\n", " ");
    }
}
