package hw_32.dto;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class OrderCreateDto {
    private final String date;
    private final List<Integer> products;
}
