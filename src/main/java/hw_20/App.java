package hw_20;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class App {
    public static void main(String[] args) {

        String pathToSave = "card.dat";
        String pathToMove = "serialized/card.dat";

        SerialWorker<Card> cardSerialWorker = new SerialWorker<>();

        saveCard(pathToSave, cardSerialWorker);

        boolean result = moveFile(pathToSave, pathToMove);
        if (result) {
            restoreCard(pathToMove, cardSerialWorker);
        }
    }

    private static void saveCard(String pathToSave, SerialWorker<Card> serialWorker) {
        Card myCard = new Card("1111 2222 3333 4444", "12/27", "Голобородько Василь","456", "9999");
        System.out.println(myCard);
        serialWorker.saveObject(myCard, pathToSave);
    }

    private static void restoreCard(String pathToRestore, SerialWorker<Card> serialWorker) {
        Card restoredCard = serialWorker.restoreObject(pathToRestore);
        System.out.println(restoredCard);
    }

    public static boolean moveFile(String sourcePath, String targetPath) {
        try {
            Files.move(Paths.get(sourcePath), Paths.get(targetPath), StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
