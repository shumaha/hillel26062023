package hw_22;

import java.util.AbstractQueue;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

public class App {
    public static void main(String[] args) {
        GasStation gasStation = new GasStation(1000, 3);
        AbstractQueue<Car> carQueue = generateQueue(50);
        gasStation.startWork(carQueue);
    }

    private static AbstractQueue<Car> generateQueue(int count) {
        // Car model array
        CarModel[] carModels = new CarModel[] {
                new CarModel("Porsche 911", 64),
                new CarModel("2019 Ford Mustang GT PREMIUM", 60),
                new CarModel("TOYOTA SUPRA BASE 2021", 52),
                new CarModel("DODGE CHALLENGER SXT 2017", 70),
                new CarModel("BMW I8 2016", 42),
                new CarModel("DODGE CHALLENGER SXT PLUS 2015", 70),
                new CarModel("PORSCHE BOXSTER S, 2014", 64),
                new CarModel("JAGUAR XK 2015", 60),
                new CarModel("CHRYSLER 300 2018", 70),
                new CarModel("MCLAREN AUTOMOTIVE MP4-12C", 72),
        };


        AbstractQueue<Car> carQueue = new ConcurrentLinkedQueue<>();

        for (int i = 0; i < count; i++) {
            CarModel carModel = carModels[getRandomInt(0, carModels.length-1)];

            carQueue.add(new Car(
                    carModel.name,
                    generatePlatNumber(),
                    carModel.fuelStorageCapacity,
                    getRandomInt(5, carModel.fuelStorageCapacity/2)
            ));
        }
        return carQueue;
    }

    public static int getRandomInt(int min, int max) {
        return new Random().nextInt(max - min + 1) + min;
    }

    public static String generatePlatNumber() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 4; i++) {
            stringBuilder.append(getRandomInt(0, 9));
        }

        return stringBuilder.toString();
    }

    private record CarModel(String name, int fuelStorageCapacity) {}
}
