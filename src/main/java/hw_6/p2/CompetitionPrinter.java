package hw_6.p2;

import java.io.IOException;
import static java.util.Objects.isNull;

public class CompetitionPrinter {
    private final int skaleRate;

    CompetitionPrinter(int skaleRate) {
        this.skaleRate = skaleRate;
    }


    public void print(String labelParticipant, Obstacle currentObstacle, Obstacle... obstacles) {
        String jumpLevelsStr = ""; // строка відображення стін на різних рівнях. Кожен рівень - це кожні 10 м.
        StringBuilder groundStr = new StringBuilder(); // строка для рівня доріжка

        // якщо null - це старт
        if (isNull(currentObstacle))
            groundStr.append(labelParticipant);

        for (Obstacle obstacle: obstacles) {
            int obstacleLength = obstacle.getlength()/skaleRate; // довжина перешкоди. для масштабування на екрані зменшуємо метри у 10 разів.

            // якщо перешкода - бігова доріжка
            if (obstacle.getClass().getSimpleName().equals("Track")) {
                groundStr.append(generateStringToFill(obstacleLength, '.'));

            }
            // перешкода - стіна
            else {
                int wallLength = obstacleLength-1; // ігноруємо елемент стіни рівня доріжка

                // якщо у стіни є вищі рівні, ніж 1 - домальовуємо нову стіну справа від вже існуючих стін
                if (wallLength > 0) {
                    jumpLevelsStr = getNewJumpLevelsStr(jumpLevelsStr, wallLength, groundStr.length());
                }
                groundStr.append('|'); // додаєм стіну у рівні доріжка
            }

            // малюємо учасника на рівні доріжки у потрібному місці
            if (obstacle == currentObstacle)
                groundStr.append(labelParticipant);
        }

        // загальна строка = N-рівнів з продовженнями стін + перший нижній рівень доріжки
        String totalStr = jumpLevelsStr+groundStr;
        clrscr();
        System.out.println(totalStr);
    }

    /**
     * Метод бере існуючий jumpLevelsStr, та додає до нього нову стіну на потрібній відстані
     */
    private String getNewJumpLevelsStr(String jumpLevelsStr, int wallLength, int groundLength) {
        String[] jumpLevelArray = getArrayFromStr(jumpLevelsStr); // отримуємо масив створених раніше стін на різних рівнях
        StringBuilder jumpLevelsStrCycle = new StringBuilder();

        // додаємо нові стіни до існуючих
        for (int i = 0; i < wallLength; i++) {
            // якщо досягли масимуму по доданим рівням стіни - додаєм нові вищі рівні
            if (i > (jumpLevelArray.length-1) ) {
                jumpLevelsStrCycle.insert(0, generateStringToFill(groundLength, ' ') + '|' + '\n');
            } else {
                int lengthToFill = groundLength - jumpLevelArray[i].length();
                String toFill = generateStringToFill(lengthToFill, ' ') + '|' + '\n';
                jumpLevelsStrCycle.insert(0, jumpLevelArray[i] + toFill);
            }
        }

        return jumpLevelsStrCycle.toString();
    }

    /**
     * Метод очищує екран
     */
    private void clrscr(){
        try {
            if (System.getProperty("os.name").contains("Windows"))
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            else
                Runtime.getRuntime().exec("clear");
        } catch (IOException | InterruptedException ignored) {}

    }

    private String generateStringToFill(int length, char needChar) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < length; i++) {
            stringBuilder.append(needChar);
        }

        return new String(stringBuilder);
    }

    private String[] getArrayFromStr(String wallStr) {
        String[] strArray = new String[0];
        if (wallStr.indexOf('\n') != -1)
            strArray = wallStr.split("\n");

        // invers array
        int len = strArray.length;
        String[] inversCopy = new String[len];
        for (int i = 0; i < len; i++) {
            inversCopy[len-1-i] = strArray[i];
        }

        return inversCopy;
    }
}
