package hw_4;

public interface Animal {
    void run(int meters);
    void swim(int meters);

}
