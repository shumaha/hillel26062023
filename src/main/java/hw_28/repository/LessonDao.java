package hw_28.repository;

import hw_28.DataBaseConnection;
import hw_28.entity.Homework;
import hw_28.entity.Lesson;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class LessonDao {

    private static final String FIND_LESSON_BY_ID =
            """
            SELECT l.id, l.name, l.updatedAt, l.homework_id, h.name as hw_name, h.description as hw_description
            FROM Lesson l
            join Homework h on l.homework_id = h.id
            where l.id = ?;
            """;

    private static final String FIND_ALL_LESSONS =
            """
            SELECT l.id, l.name, l.updatedAt, l.homework_id, h.name as hw_name, h.description as hw_description
            FROM Lesson l
            join Homework h on l.homework_id = h.id
            """;
    private static final String INSERT_LESSON = "INSERT INTO Lesson(name, updatedAt, homework_id) VALUES(?, ?, ?)";
    private static final String INSERT_HOMEWORK = "INSERT INTO Homework(name, description) VALUES(?, ?);";

    private static final String FIND_HOMEWORK_BY_NAME =
         """
         SELECT id from Homework WHERE name = ?
         ORDER BY id DESC
         LIMIT 1;
         """;
    private static final String DELETE_LESSON_BY_ID = "DELETE FROM Lesson WHERE id = ?";
    private static final String DELETE_HOMEWORK_BY_ID = "DELETE FROM Homework WHERE id = ?";

    public boolean addLesson(Lesson lesson) {
        int rowAdd = 0;

        try {
            Connection connection = DataBaseConnection.getConnection();

            //insert Homework
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_HOMEWORK);

            int indexCount = 1;
            preparedStatement.setString(indexCount++, lesson.getHomework().getName());
            preparedStatement.setString(indexCount, lesson.getHomework().getDescription());

            rowAdd = preparedStatement.executeUpdate();

            if (rowAdd > 0) {

                // SELECT homework id
                preparedStatement = connection.prepareStatement(FIND_HOMEWORK_BY_NAME);
                preparedStatement.setString(1, lesson.getHomework().getName());;

                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    int hw_id = Integer.parseInt(resultSet.getString("id"));

                    // INSERT Lesson
                    preparedStatement = connection.prepareStatement(INSERT_LESSON);

                    indexCount = 1;
                    preparedStatement.setString(indexCount++, lesson.getName());
                    preparedStatement.setString(indexCount++, lesson.getUpdatedAt());
                    preparedStatement.setInt(indexCount, hw_id);

                    rowAdd = preparedStatement.executeUpdate();

                    return rowAdd > 0;
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Exception occurred when tries to add lesson!");
        }

        return false;
    }

    public boolean deleteLesson(int idLesson) {
        int rowDelete = 0;

        try {
            Connection connection = DataBaseConnection.getConnection();

            // Find ID homework by lesson ID
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_LESSON_BY_ID);

            preparedStatement.setInt(1, idLesson);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                int hw_id = resultSet.getInt("homework_id");

                // delete Lesson
                preparedStatement = connection.prepareStatement(DELETE_LESSON_BY_ID);
                preparedStatement.setInt(1, idLesson);

                rowDelete = preparedStatement.executeUpdate();

                if (rowDelete > 0) {
                    // delete Homework
                    preparedStatement = connection.prepareStatement(DELETE_HOMEWORK_BY_ID);
                    preparedStatement.setInt(1, hw_id);

                    rowDelete = preparedStatement.executeUpdate();

                    return rowDelete > 0;
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Exception occurred when tries to delete lesson!");
        }

        return false;

    }

    public List<Lesson> getLessons() {
        List<Lesson> lessons = new ArrayList<>();

        try {
            Connection connection = DataBaseConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_LESSONS);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int homework_id = resultSet.getInt("homework_id");
                String hw_name = resultSet.getString("hw_name");
                String hw_description = resultSet.getString("hw_description");

                Homework homework = new Homework(homework_id, hw_name, hw_description);

                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String updatedAt = resultSet.getString("updatedAt");

                Lesson lesson = new Lesson(id, name, updatedAt, homework);

                lessons.add(lesson);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Exception occurred when tries to get lessons!");
        }

        return lessons;
    }

    public Optional<Lesson> getLessonById(int idLesson) {

        try {
            Connection connection = DataBaseConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_LESSON_BY_ID);
            preparedStatement.setInt(1, idLesson);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                int homework_id = resultSet.getInt("homework_id");
                String hw_name = resultSet.getString("hw_name");
                String hw_description = resultSet.getString("hw_description");

                Homework homework = new Homework(homework_id, hw_name, hw_description);

                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String updatedAt = resultSet.getString("updatedAt");

                Lesson lesson = new Lesson(id, name, updatedAt, homework);
                return Optional.of(lesson);
            }

            return Optional.empty();

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Exception occurred when tries to find lesson by ID!");
        }
    }

}
