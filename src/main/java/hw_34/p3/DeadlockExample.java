package hw_34.p3;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DeadlockExample {
    private Lock lock1 = new ReentrantLock();
    private Lock lock2 = new ReentrantLock();

    private final HashMap<Lock, String> locksInfo = new HashMap<>();

    /*{
        int lock1Hash = System.identityHashCode(lock1);
        int lock2Hash = System.identityHashCode(lock2);
        System.out.println("Lock1 hash: " + lock1Hash + ", Lock2 hash: " + lock2Hash);
        locksInfo.put(lock1, "Lock1");
    }*/

    public DeadlockExample() {
        locksInfo.put(lock1, "Lock1");
        locksInfo.put(lock2, "Lock2");
    }

    public void runExampleWithDeadblock() {
        Thread thread1 = new Thread(() -> {
            lock1.lock();
            System.out.println(Thread.currentThread() + " locks lock1");
            try {
                try {
                    lock2.lock();
                    System.out.println(Thread.currentThread() + " locks lock2");
                } finally {
                    lock2.unlock();
                    System.out.println(Thread.currentThread() + " unlocks lock2");
                }
            } finally {
                lock1.unlock();
                System.out.println(Thread.currentThread() + " unlocks lock1");
            }
        });
        thread1.setName("Thread1");

        Thread thread2 = new Thread(() -> {
            lock2.lock();
            System.out.println(Thread.currentThread() + " locks lock2");
            try {
                try {
                    lock1.lock();
                    System.out.println(Thread.currentThread() + " locks lock1");
                } finally {
                    lock1.unlock();
                    System.out.println(Thread.currentThread() + " unlocks lock1");
                }
            } finally {
                lock2.unlock();
                System.out.println(Thread.currentThread() + " unlocks lock2");
            }
        });
        thread2.setName("Thread2");

        thread1.start();
        thread2.start();
    }

    public void runExampleFixedDeadblock() {
        Runnable orderedBlockRunnable = () -> {
            Lock lockSecond = lock2;
            Lock lockFirst = lock1;

            if (lock2.hashCode() < lock1.hashCode()) {
                lockFirst = lock2;
                lockSecond = lock1;
            }


            System.out.println(Thread.currentThread() + " locks first [" + locksInfo.get(lockFirst) + "]");
            lockFirst.lock();
            try {
                try {
                    System.out.println(Thread.currentThread() + " locks second [" + locksInfo.get(lockSecond) + "]");
                    lockSecond.lock();
                } finally {
                    System.out.println(Thread.currentThread() + " unlocks second [" + locksInfo.get(lockSecond) + "]");
                    lockSecond.unlock();
                }
            } finally {
                System.out.println(Thread.currentThread() + " unlocks first [" + locksInfo.get(lockFirst) + "]");
                lockFirst.unlock();
            }
        };

        Thread thread1 = new Thread(orderedBlockRunnable, "Thread1");
        Thread thread2 = new Thread(orderedBlockRunnable, "Thread2");

        thread1.start();
        thread2.start();
    }
}
