package hw_18.observer;

import hw_18.observer.interfaces.Observer;

import java.time.format.DateTimeFormatter;

public class MessengerAccount implements Observer<AlarmEvent> {
    private static long totalCount = 1;
    private final long id;

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public MessengerAccount() {
        id = totalCount++;
    }

    @Override
    public void handleEvent(AlarmEvent alarmEvent) {

        String alarmType = "";
        if (alarmEvent.isAlarmStart())
            alarmType = "Повітряна тривога";
        else
            alarmType = "Відбій повітряної тривоги";

        System.out.println("AccountID["+id+"] receives event: \"Увага! " + alarmType + " у областях " + alarmEvent.printRegions()
                + "! Час події: " + formatter.format(alarmEvent.getEventTime())+"\"");
    }
}
