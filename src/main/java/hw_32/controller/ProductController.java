package hw_32.controller;

import hw_32.entity.Product;
import hw_32.repository.DataBaseConnection;
import hw_32.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    DataBaseConnection dataBaseConnection;

    @Autowired
    ProductService productService;

    @GetMapping("/show")
    public List<Product> showProducts() {

        return productService.showAll();
    }

    @GetMapping("/show/{id}")
    public Product showProduct(@PathVariable("id") int idProduct) {

        return productService.findProductById(idProduct);
    }
}
