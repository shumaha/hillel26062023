package hw_5;

public class App {

    public static void main(String[] args) {

        Pizza.PizzaBuilder pizzaBuilder = Pizza.builder(Pizza.CheeseType.DORBLU, false);

        Pizza pizza = pizzaBuilder
                .meat(Pizza.MeatType.CHIKEN, false)
                .tomato()
//                .sauce()
                .build();

        System.out.println(pizza);
    }
}
