package hw_9.p2;

public class FruitWorker {

    public static void workWithFruit() {

        // create Box with apples
        Box<Apple> appleBox = new Box<>(Box.BoxSize.NORMAL);
        appleBox.add(generateApples(9));
        appleBox.add(new Apple()); // add one fruit
        appleBox.add(new Apple(), new Apple()); // add two fruit
        System.out.println("Box with apples: " + appleBox + ", Box weight: " + appleBox.getWeight());

        // Create Box with oranges
        Box<Orange> orangeBox = new Box<>(Box.BoxSize.BIG);
        orangeBox.add(generateOranges(7));
        orangeBox.add(new Orange()); // add one fruit
        System.out.println("Box with oranges: " + orangeBox + ", box weight: " + orangeBox.getWeight());

        // Compare boxes
        System.out.println("compare(): apple box VS Orange box: " + appleBox.compare(orangeBox));

        // Merge boxes
        // appleBox.merge(orangeBox); // error
        Box<Apple> appleBoxToMerge = new Box<>(Box.BoxSize.LITTLE);
        appleBoxToMerge.add(generateApples(40));
        System.out.println("Result of merge: " + appleBox.merge(appleBoxToMerge));
        System.out.println("appleBox after merge(): " + appleBox + ", Box weight: " + appleBox.getWeight());
        System.out.println("appleBoxToMerge after merge(): " + appleBoxToMerge + ", Box weight: " + appleBoxToMerge.getWeight());
    }

    public static Apple[] generateApples(int cnt) {
        Apple[] fruits = new Apple[cnt];
        for (int i = 0; i < cnt; i++) {
            fruits[i] = new Apple();
        }

        return fruits;
    }

    public static Orange[] generateOranges(int cnt) {
        Orange[] fruits = new Orange[cnt];
        for (int i = 0; i < cnt; i++) {
            fruits[i] = new Orange();
        }

        return fruits;
    }
}
